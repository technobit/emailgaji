<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Pot_pajak extends MX_Controller {
	private $input_file_name = 'lampiran';
	private $import_dir = 'assets/import';

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'POT-PAJAK';
		$this->module   = 'pajak';
		$this->routeURL = 'pot_pajak';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('Pot_pajak_model', 'model');

    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Data Bukti Potongan Pajak';
		$this->page->menu 	  = 'bupot';
		$this->page->submenu1 = 'pot_pajak';
		$this->breadcrumb->title = 'Data Bukti Potongan Pajak';
		$this->breadcrumb->card_title = 'Riwayat Bukti Potongan Pajak';
		$this->breadcrumb->icon = 'fas fa-file-invoice';
		$this->breadcrumb->list = ['Bukti Potongan Pajak'];
		$this->js = true;
		$this->css = true;
		$data['url'] = site_url("{$this->routeURL}/add");
		$this->render_view('pot_pajak/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$totald = $this->model->listCount($this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			$data[] = array($i.'. ', idn_date($d->import_date, "j F Y H:i:s"), $d->file_name, $d->total, idn_date($d->dt_email_dikirim), $d->created, $d->updated, $d->h_id);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $totald,
								'iTotalDisplayRecords' => $totald,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()));
	}

	public function add(){
		if($this->authCheckDetailAccess('c', true) == false) return; // hak akses untuk modal popup
		$email = $this->model->get_last_email(1);

		$data['data']		= $email;
		$data['url']        = site_url("{$this->routeURL}/save");
		$data['title']      = 'Import Data Bukti Potongan Pajak';
		$data['input_file_name'] = $this->input_file_name;
		$this->load_view('pot_pajak/import_action', $data, true);
		
	}

	public function save(){
		$this->authCheckDetailAccess('c');
		
		$start = start_time();
		$this->form_validation->set_rules('mulai', 'Mulai', 'required|integer');
		$data_post						= $this->input->post();
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'mc' => false, //modal close
								'msg' => "Terjadi kesalahan",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        }else{
			$upload_msg 	= [];
			$files 			= [];
			$excel_up_stat	= true;
			$img_up_stat	= true;
			if(isset($data_post['cdImg'])){
				$cdImg = $data_post['cdImg'];
			}

			if(isset($_FILES['var_ttd_stempel'])){
				$upload_image = $this->upl_image('var_ttd_stempel', $cdImg['ttd_stempel']);
				if(isset($upload_image['var_image'])){
					$data_post['var_ttd_stempel'] = $upload_image['var_image'];
				}else{
					$img_up_stat = false;
				}
				$upload_msg[$cdImg['ttd_stempel']] = $upload_image['upload_msg'];
			}
			$files = ['files' => $upload_msg];


			if(isset($_FILES[$this->input_file_name])){
				$config_xslx['upload_path']   = "./{$this->import_dir}"; 
				$config_xslx['allowed_types'] = 'xlsx|xls'; 
				$config_xslx['encrypt_name']  = true; 
				$config_xslx['max_size']      = 4096;  
				$this->load->library('upload', $config_xslx);
				$upl_xlsx = $this->upload->do_upload($this->input_file_name);
				
				if(($upl_xlsx) && ($img_up_stat)){

					$status  = $this->model->import($data_post, $this->upload->data());
					//$check = $this->model->create($data_post);
					if($status==true){
						$this->set_json([  'stat' => ($status !== false), 
											'mc' => ($status !== false), //modal close
											'time' => finish_time($start),
											'msg' => ($status !== false)? "Data berhasil di-import dengan {$status} baris data." : 'Data gagal di-import',
											'csrf' => [ 'name' => $this->getCsrfName(),
														'token' => $this->getCsrfToken()]
										]);
					}else{
						$this->set_json([  'stat' => false, 
											'mc' => false, //modal close
											'msg' => "Data gagal di-import. ".$this->upload->display_errors('','')." | Error Database Query",
											'csrf' => [ 'name' => $this->getCsrfName(),
														'token' => $this->getCsrfToken()]
										]);
					}
				} else {
					$this->set_json([  'stat' => false, 
										'mc' => false, //modal close
										'xlsx' => $upl_xlsx,
										'msg' => "Data gagal di-import. ".$this->upload->display_errors('','')." | Error Upload Files",
										'csrf' => [ 'name' => $this->getCsrfName(),
													'token' => $this->getCsrfToken()]
									]);
				}
			} else {
				$this->set_json([  'stat' => false, 
									'mc' => false, //modal close
									'msg' => "Data gagal di-import | Files Not Found",
									'csrf' => [ 'name' => $this->getCsrfName(),
                                            	'token' => $this->getCsrfToken()]
								]);
			}

        }
	}

	public function get($h_id){
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($h_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Error', 'title' => 'posts Not Found', 'message' => '']],true);
		}else{
			$data['data'] 	= $res;
			$data['url']	= site_url("{$this->routeURL}/$h_id");
			$data['title']	= 'Edit Data';
			$this->load_view('pot_pajak/edit_action', $data);
		}
		
	}

	public function update($h_id){
		$this->authCheckDetailAccess('u');
		
		$this->form_validation->set_rules('txt_category', 'Category', 'alpha_numeric_spaces|min_length[4]|max_length[100]');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
            $check = $this->model->update($h_id, $this->input_post());
			$this->set_json([  'stat' => $check, 
								'mc'   => $check, //modal close
								'msg'  => ($check)? "Data Updated Successfully" : "Data Update Failed",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);

        }
	}

	public function confirm($h_id){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($h_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['url']	= site_url("{$this->routeURL}/$h_id/del");
			$data['title']	= 'Hapus Data Penggajian';
			$data['info']   = [ 'Tanggal Upload' => idn_date($res->import_date, "j F Y H:i:s"),
                                'Total Data' => $res->total,
                                'Jadwal Kirim' => idn_date($res->dt_email_dikirim)];
			$this->load_view('pot_pajak/delete_confirm', $data);
		}
	}

	public function delete($h_id){
		$this->authCheckDetailAccess('d');

		$check = $this->model->delete($h_id);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Data Deleted Successfully" : "Data Delete Failed",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
		
	}

	public function detail($id_import){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Data Penggajian';
		$this->page->menu 	  = 'gaji';
		//$this->page->submenu1 = 'penggajian';
		$this->breadcrumb->title = 'Data Pengajian';
		$this->breadcrumb->card_title = 'Riwayat Data Penggajian';
		$this->breadcrumb->icon = 'fas fa-money-bill-wave-alt';
		$this->breadcrumb->list = ['Penggajian'];
		$this->js = true;
		$data['url'] = site_url("{$this->routeURL}/add");
		$this->render_view('pot_pajak/index', $data, true);
	}

	public function detail_list($id_import){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$totald = $this->model->listCount($this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			$data[] = array($i.'. ', idn_date($d->import_date, "j F Y H:i:s"), $d->file_name, $d->total, idn_date($d->dt_email_dikirim), $d->h_id);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $totald,
								'iTotalDisplayRecords' => $totald,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()));
	}
	
	public function upl_excel($input_excel, $input_excel_name){

		$config_xslx['upload_path']   = "./{$this->import_dir}"; 
		$config_xslx['allowed_types'] = 'xlsx|xls'; 
		$config_xslx['encrypt_name']  = true; 
		$config_xslx['max_size']      = 4096;  

		$this->load->library('upload', $config_xslx);
		
		if($this->upload->do_upload($input_excel_name)){
			$ret['upload_msg'] = ['stat' => true, 'msg' => 'Upload Success'];
			$ret['var_image'] = $img_dir.'/'.$this->upload->data('file_name');

			//$ret['img_up_stat'] = true;
		}else{
			$ret['upload_msg'] = ['stat' => false, 'msg' => $this->upload->display_errors('','')];
			$ret['excel_up_stat'] = false;
		}
		$this->upload->error_msg = [];

		return $ret;
	}

	public function upl_image($input_img_name, $cdImg){

		$config_img['upload_path']   = "./{$this->import_dir}"; 
		$config_img['allowed_types'] = 'jpg|png|jpeg|xlsx|xls'; 
		$config_img['encrypt_name']  = $this->config->item('encrypt_name');
		$config_img['max_size']      = $this->config->item('max_size');

		$this->load->library('upload', $config_img);
		$upload_image = $this->upload->do_upload($input_img_name);
		
		if($upload_image){
			$file_name = $this->upload->data('file_name');
			$ret['var_image'] = $this->import_dir.'/'.$this->upload->data('file_name');
			$ret['upload_msg'] = ['stat' => true, 'msg' => 'Upload Success', 'cdImg' => $cdImg];
			//$ret['img_up_stat'] = true;
		}else{
			$ret['upload_msg'] = ['stat' => false, 'msg' => $this->upload->display_errors('',''), 'cdImg' => $cdImg];
			$ret['img_up_stat'] = false;
		}
		$this->upload->error_msg = [];

		return $ret;
	}
}
