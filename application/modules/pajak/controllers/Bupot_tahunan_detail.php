<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
	
class Bupot_tahunan_detail extends MX_Controller {

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'BUPOT-TAHUNAN';
		$this->module   = 'pajak';
		$this->routeURL = 'bupot_tahunan_detail';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('Bupot_tahunan_detail_model', 'model');

    }
	
	public function index($id_import){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Daftar Bukti Potongan Pajak Tahunan';
		$this->page->menu 	  = 'bupot';
		$this->page->submenu1 = 'bupot_tahunan';
		$this->breadcrumb->title = 'Daftar Bukti Potongan Pajak Tahunan';
		$this->breadcrumb->card_title = 'Daftar Bukti Potongan Pajak Tahunan';
		$this->breadcrumb->icon = 'fas fa-money-bill-wave-alt';
		$this->breadcrumb->list = ['Bukti Potongan Pajak', 'Laporan Tahunan', 'Daftar Laporan Pajak'];
		$this->js = true;
		$data['detail'] = $this->model->get_import($id_import);
		$data['list_detail_url'] = site_url("{$this->routeURL}/{$id_import}");
		$data['url'] = site_url("{$this->routeURL}/add");
		$data['url_resend_all'] = site_url("{$this->routeURL}/resendall/{$id_import}");
		$data['url_back'] = site_url("bupot_tahunan");
		$data['url_export'] = site_url("export/{$this->routeURL}");
		$this->render_view('bupot_tahunan_detail/index', $data, true);
	}

	public function list($id_import){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$totald = $this->model->listCount($id_import, $this->input->post('status_filter', true), $this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($id_import, $this->input->post('status_filter', true), $this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i		= $this->input_post('start', true);
		foreach($ldata as $dt){
			$action = '<a href="'.site_url("{$this->routeURL}/").$dt->int_potongan_pajak_id.'/bupot" class="btn btn-sm btn-primary tooltips"><i class="fas fa-receipt"></i></a>';
			if($dt->int_status == 3 || $dt->int_status == 4){
				$action .= '<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}/").$dt->int_potongan_pajak_id.'/resend" class="ajax_modal btn btn-sm btn-warning tooltips" data-placement="top" data-original-title="Resend" style="margin-left:5px"><i class="fas fa-sync-alt"></i></a>';
			}
			$i++;
			$data[] = array($i.'. ', $dt->var_nik, $dt->var_nama, $dt->var_npwp, $dt->var_email, idn_date($dt->dt_email_dikirim), $dt->int_status, $action);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $totald,
								'iTotalDisplayRecords' => $totald,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()),
							200, false);
	}

	public function confirm($int_potongan_pajak_id){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get_bupot($int_potongan_pajak_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['url']	= site_url("{$this->routeURL}/$int_potongan_pajak_id/resend");
			$data['title']	= 'Kirim Ulang Bukti Potongan Pajak';
			$data['info']   = [ 'NIK' => $res->var_nik_ktp,
                                'Nama' => $res->var_nama,
								'NPWP' => $res->var_npwp,
								'Email' => $res->var_email,
								'Tanggal Kirim' => idn_date($res->dt_email_dikirim)
							];
			$this->load_view('bupot_tahunan_detail/resend_confirm', $data);
		}
	}

	public function resend($int_potongan_pajak_id){
		$this->authCheckDetailAccess('d');

		$check = $this->model->resend($int_potongan_pajak_id);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Email Resend Successfully" : "Email Failed to Resend",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
		
	}

	public function confirm_resendall($id_import){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get_import($id_import);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['url']	= site_url("{$this->routeURL}/resendall/{$id_import}");
			$data['title']	= 'Kirim Ulang Semua yang Belum Terkirim?';
			$data['info']   = [ 'Tanggal Upload' => idn_date($res->import_date, "j F Y H:i:s"),
                                'Periode' => idn_date($res->dt_periode, "F Y"),
                                'Jadwal Kirim' => idn_date($res->dt_email_dikirim)];
			$this->load_view('bupot_tahunan_detail/resendall_confirm', $data);
		}
	}

	public function resendall($id_import){
		$this->authCheckDetailAccess('d');

		$check = $this->model->resendall($id_import);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Email Resend Successfully" : "Email Failed to Resend",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
		
	}

	public function get_bupot($int_potongan_pajak_id){
		if($this->authCheckDetailAccess('u', true) == false) return;
		$this->page->subtitle = 'Form Bukti Potongan Pajak Tahunan';
		$this->page->menu 	  = 'bupot';
		$this->page->submenu1 = 'bupot_tahunan';
		$this->css = true;
		$this->breadcrumb->title = 'Form Bukti Potongan Pajak Tahunan';
		$this->breadcrumb->list = ['Bukti Potongan Pajak', 'Laporan Tahunan', 'Daftar Laporan Pajak'];

		$res = $this->model->get_bupot($int_potongan_pajak_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Error', 'title' => 'posts Not Found', 'message' => '']],true);
		}else{
			$data['title']	= 'Form Bukti Potongan Pajak';
			$data['url'] = site_url("{$this->routeURL}/{$res->id_import}");
			$data['data'] 	= $res;

		}
		$this->render_view('bupot_tahunan_detail/form_bupot', $data);
		
	}

	public function export($id_import){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page
		
		$per_tanggal = $this->input->post('tanggal_filter');

		$datagaji = $this->model->get_import($id_import);
		$ldata = $this->model->list($id_import, $this->input->post('status_filter', true));

        $title    = 'Data Penerima Gaji';

        $filename = 'Data Penerima Gaji - Periode '.idn_date($detail->dt_periode, "F Y");


		$input_file = 'assets/export/data_gaji.xlsx';
		/** Load $inputFileName to a Spreadsheet object **/
		//$spreadsheet = new Spreadsheet();
		$spreadsheet = PhpOffice\PhpSpreadsheet\IOFactory::load($input_file);
		$spreadsheet->setActiveSheetIndex(0)
					->setCellValue('A1', 'Daftar Penerima Gaji Periode '.idn_date($detail->dt_periode, "F Y"));

		$sheet = $spreadsheet->getActiveSheet();

        $i = 0;
        $x = 3;
        foreach($ldata as $d){
            $i++;
			$x++;

			switch ($d->int_status) {
				case 1:
					$status = 'Belum Dikirim';
					break;
				case 2:
					$status = 'Proses Pengiriman';
					break;
				case 3:
					$status = 'Belum Dikirim';
					break;
				case 4:
					$status = 'Terkirim';
					break;
				default:
					$status = 'Gagal Dikirim';
		  } 

            $sheet->setCellValue('A'.$x, $i);
			$sheet->setCellValueExplicit('B'.$x, $d->a,\PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $sheet->setCellValue('C'.$x, $d->b);
            $sheet->setCellValue('D'.$x, $d->d);
            $sheet->setCellValue('E'.$x, $d->bv);
            $sheet->setCellValue('F'.$x, $status);
        }
		
		$sheet->setTitle($title);
        $spreadsheet->setActiveSheetIndex(0);

        $this->set_header($filename);

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		if (ob_get_contents()) ob_end_clean();
		$writer->save('php://output');
		exit;
	}
}
