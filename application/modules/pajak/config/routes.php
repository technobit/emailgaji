<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['bupot_bulanan']['get']                       = 'pajak/bupot_bulanan';
$route['bupot_bulanan']['post']                      = 'pajak/bupot_bulanan/list';
$route['bupot_bulanan/add']['get']                   = 'pajak/bupot_bulanan/add';
$route['bupot_bulanan/save']['post']                 = 'pajak/bupot_bulanan/save';
$route['bupot_bulanan/([a-zA-Z0-9]+)']['get']        = 'pajak/bupot_bulanan/get/$1';
$route['bupot_bulanan/([a-zA-Z0-9]+)']['post']       = 'pajak/bupot_bulanan/update/$1';
$route['bupot_bulanan/([a-zA-Z0-9]+)/del']['get']    = 'pajak/bupot_bulanan/confirm/$1';
$route['bupot_bulanan/([a-zA-Z0-9]+)/del']['post']   = 'pajak/bupot_bulanan/delete/$1';

$route['bupot_bulanan_detail/([a-zA-Z0-9]+)']['get']         = 'pajak/bupot_bulanan_detail/index/$1';
$route['bupot_bulanan_detail/([a-zA-Z0-9]+)']['post']        = 'pajak/bupot_bulanan_detail/list/$1';
$route['bupot_bulanan_detail/([a-zA-Z0-9]+)/bupot']['get']   = 'pajak/bupot_bulanan_detail/get_bupot/$1';
$route['bupot_bulanan_detail/([a-zA-Z0-9]+)/resend']['get']  = 'pajak/bupot_bulanan_detail/confirm/$1';
$route['bupot_bulanan_detail/([a-zA-Z0-9]+)/resend']['post'] = 'pajak/bupot_bulanan_detail/resend/$1';
$route['bupot_bulanan_detail/resendall/([a-zA-Z0-9]+)']['get']   = 'pajak/bupot_bulanan_detail/confirm_resendall/$1';
$route['bupot_bulanan_detail/resendall/([a-zA-Z0-9]+)']['post']  = 'pajak/bupot_bulanan_detail/resendall/$1';
$route['export/bupot_bulanan_detail/([a-zA-Z0-9]+)']['post']     = 'pajak/bupot_bulanan_detail/export/$1';

$route['bupot_tahunan']['get']                       = 'pajak/bupot_tahunan';
$route['bupot_tahunan']['post']                      = 'pajak/bupot_tahunan/list';
$route['bupot_tahunan/add']['get']                   = 'pajak/bupot_tahunan/add';
$route['bupot_tahunan/save']['post']                 = 'pajak/bupot_tahunan/save';
$route['bupot_tahunan/([a-zA-Z0-9]+)']['get']        = 'pajak/bupot_tahunan/get/$1';
$route['bupot_tahunan/([a-zA-Z0-9]+)']['post']       = 'pajak/bupot_tahunan/update/$1';
$route['bupot_tahunan/([a-zA-Z0-9]+)/del']['get']    = 'pajak/bupot_tahunan/confirm/$1';
$route['bupot_tahunan/([a-zA-Z0-9]+)/del']['post']   = 'pajak/bupot_tahunan/delete/$1';

$route['bupot_tahunan_detail/([a-zA-Z0-9]+)']['get']         = 'pajak/bupot_tahunan_detail/index/$1';
$route['bupot_tahunan_detail/([a-zA-Z0-9]+)']['post']        = 'pajak/bupot_tahunan_detail/list/$1';
$route['bupot_tahunan_detail/([a-zA-Z0-9]+)/bupot']['get']   = 'pajak/bupot_tahunan_detail/get_bupot/$1';
$route['bupot_tahunan_detail/([a-zA-Z0-9]+)/resend']['get']  = 'pajak/bupot_tahunan_detail/confirm/$1';
$route['bupot_tahunan_detail/([a-zA-Z0-9]+)/resend']['post'] = 'pajak/bupot_tahunan_detail/resend/$1';
$route['bupot_tahunan_detail/resendall/([a-zA-Z0-9]+)']['get']   = 'pajak/bupot_tahunan_detail/confirm_resendall/$1';
$route['bupot_tahunan_detail/resendall/([a-zA-Z0-9]+)']['post']  = 'pajak/bupot_tahunan_detail/resendall/$1';
$route['export/bupot_tahunan_detail/([a-zA-Z0-9]+)']['post']     = 'pajak/bupot_tahunan_detail/export/$1';

$route['pot_pajak']['get']                       = 'pajak/pot_pajak';
$route['pot_pajak']['post']                      = 'pajak/pot_pajak/list';
$route['pot_pajak/add']['get']                   = 'pajak/pot_pajak/add';
$route['pot_pajak/save']['post']                 = 'pajak/pot_pajak/save';
$route['pot_pajak/([a-zA-Z0-9]+)']['get']        = 'pajak/pot_pajak/get/$1';
$route['pot_pajak/([a-zA-Z0-9]+)']['post']       = 'pajak/pot_pajak/update/$1';
$route['pot_pajak/([a-zA-Z0-9]+)/del']['get']    = 'pajak/pot_pajak/confirm/$1';
$route['pot_pajak/([a-zA-Z0-9]+)/del']['post']   = 'pajak/pot_pajak/delete/$1';

$route['detail_potong_pajak/([a-zA-Z0-9]+)']['get']         = 'pajak/detail/index/$1';
$route['detail_potong_pajak/([a-zA-Z0-9]+)']['post']        = 'pajak/detail/list/$1';
$route['detail_potong_pajak/([a-zA-Z0-9]+)/bupot']['get']   = 'pajak/detail/get_bupot/$1';
$route['detail_potong_pajak/([a-zA-Z0-9]+)/resend']['get']  = 'pajak/detail/confirm/$1';
$route['detail_potong_pajak/([a-zA-Z0-9]+)/resend']['post'] = 'pajak/detail/resend/$1';
$route['detail_potong_pajak/resendall/([a-zA-Z0-9]+)']['get']   = 'pajak/detail/confirm_resendall/$1';
$route['detail_potong_pajak/resendall/([a-zA-Z0-9]+)']['post']  = 'pajak/detail/resendall/$1';
$route['export/detail_potong_pajak/([a-zA-Z0-9]+)']['post']     = 'pajak/detail/export/$1';