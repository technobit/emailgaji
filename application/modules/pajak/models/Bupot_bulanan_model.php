<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;

class Bupot_bulanan_model extends MY_Model {

	public function get_last_email($int_type){
		return $this->db->query("SELECT txt_isi_email, var_subjek_email
								 FROM	{$this->h_import}
								 WHERE int_type = {$int_type}
								 ORDER BY h_id DESC LIMIT 1")->row();
	}

    public function list($filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$this->db->select("*, CONCAT(suc.txt_nama_depan, ' ', suc.txt_nama_belakang) as created,
		CONCAT(suu.txt_nama_depan, ' ', suu.txt_nama_belakang) as updated")
					->from($this->h_import." hi")
					->join($this->s_user." suc", "hi.created_by = suc.int_id_user", "left")
					->join($this->s_user." suu", "hi.updated_by = suu.int_id_user", "left")
					->where('hi.int_type', 3); //bupot bulanan 2024


		if(($this->session->userdata['int_level'] == 2)){
			$this->db->where('hi.int_level', 2);
		}
			
		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('import_date', $filter)
					->or_like('file_name', $filter)
					->or_like('total', $filter)
					->or_like('dt_email_dikirim', $filter)
					->group_end();
		}

		switch($order_by){
			case 1 : $order = 'import_date '; break;
			case 2 : $order = 'file_name '; break;
			case 3 : $order = 'total '; break;
			case 4 : $order = 'dt_email_dikirim '; break;
			default: {$order = 'import_date '; $sort = 'DESC';} break;

		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($filter = NULL){
		$this->db->from($this->h_import)
					->where('int_type', 3);  //bupot bulanan 2024

        if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
            $this->db->group_start()
			->like('import_date', $filter)
			->or_like('file_name', $filter)
			->or_like('total', $filter)
			->or_like('dt_email_dikirim', $filter)
			->group_end();
        }
		return $this->db->count_all_results();
	}

	public function get($h_id){
		return $this->db->select("hi.*, tp.dt_periode")
					->join($this->t_potongan_pajak_bulanan.' tp', 'hi.`h_id` = tp.`id_import`', 'left')
					->get_where($this->h_import.' hi', ['h_id' => $h_id])->row();
	}

	public function update($h_id, $upd){
		$upd_import['var_subjek_email'] = $upd['var_subjek_email'];
		$upd_import['txt_isi_email'] = $upd['txt_isi_email'];
		$upd_import['dt_email_dikirim'] = $upd['dt_email_dikirim'];
		$upd_import['updated_by'] = $this->session->userdata['user_id'];
		$upd_bupot_bulanan['dt_periode'] = $upd['dt_periode'];
		$upd_bupot_bulanan['dt_email_dikirim'] = $upd['dt_email_dikirim'];

		$this->db->trans_begin();

		$this->db->where('h_id', $h_id);
		$this->db->update($this->h_import, $upd_import);

		$upd_data_bupot_bulanan = $this->upd_data_bupot_bulanan($h_id, $upd_bupot_bulanan);

		if ($this->db->trans_status() === FALSE|| $upd_data_bupot_bulanan === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function upd_data_bupot_bulanan($id_import, $upd){
		$this->db->trans_begin();

		$this->db->where('id_import', $id_import);
		$this->db->update($this->t_potongan_pajak_bulanan, $upd);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete($h_id){
		$this->db->trans_begin();
		$this->db->delete($this->h_import,  ['h_id' => $h_id]);

		$del_data_bupot_bulanan = $this->del_data_bupot_bulanan($h_id);

		if ($this->db->trans_status() === FALSE || $del_data_bupot_bulanan === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function del_data_bupot_bulanan($id_import){
		$this->db->trans_begin();
		$this->db->delete($this->t_potongan_pajak_bulanan,  ['id_import' => $id_import]);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function import($ins, $file){
		$user 	= $this->session->userdata('username');
		$dt_periode = $ins['dt_periode'];
		$dt_email_dikirim = $ins['dt_email_dikirim'];
		$var_ttd_stempel = $ins['var_ttd_stempel'];
		$created_by = $this->session->userdata['user_id'];
		$int_level = $this->session->userdata['int_level'];

		$int_status = 2;
		$a = 'A'; $b = 'B'; $c = 'C'; $d = 'D'; $e = 'E'; $f = 'F'; $g = 'G'; $h = 'H'; $i = 'I'; $j = 'J';
		$k = 'K'; $l = 'L'; $m = 'M'; $n = 'N'; $o = 'O'; $p = 'P'; $q = 'Q'; $r = 'R'; $s = 'S'; $t = 'T';
		$u = 'U';


		$filterSubset = new MyReadFilter($ins['mulai'],
						[$a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $n, $o, $p, $q, $r, $s, $t, $u]
					);

		$reader = IOFactory::createReader(ucfirst(ltrim($file['file_ext'],'.')));
		$reader->setReadDataOnly(true);
		$reader->setReadFilter($filterSubset);
		$spreadsheet = $reader->load($file['full_path']);
		$data = $spreadsheet->getActiveSheet()->toArray(null, true, false, true);
		
		$this->db->trans_begin();
		
		$this->db->insert($this->h_import, ['username' => $user,
										  'file_name' => $file['orig_name'],
										  'direktori' => $file['full_path'],
										  'var_subjek_email' => $ins['var_subjek_email'],
										  'txt_isi_email' => $ins['txt_isi_email'],
										  'dt_email_dikirim' => $ins['dt_email_dikirim'],
										  'created_by' => $created_by,
										  'int_level' => $int_level,
										  'int_type' => 3
										  ]);//'int_type' => 3 bupot bulanan 2024
										  
		$id_import = $this->db->insert_id();
		$ins_data_bupot_bulanan = "INSERT INTO {$this->t_potongan_pajak_bulanan} 
						(`dt_email_dikirim`, `var_nik`, `var_nama`, `var_email`, `var_no_bukti_potong`, `var_masa_pajak`, `var_tahun_pajak`,
						`var_npwp`, `var_nik_ktp`, `var_alamat`, `var_kode_pajak`,
						`dec_jumlah_penghasilan_bruto`, `dec_dasar_pengenaan_pajak`, `var_tarif_tidak_ber_npwp`, `dec_tarif`, `dec_pph_dipotong`,
						`var_no_dokumen_ref`, `var_npwp_pemotong`, `var_nama_pemotong`, `var_nama_penandatangan`, `dt_tgl_bukti_potong`,
						`var_ttd_stempel`, `int_status`, `dt_periode`, `id_import`) VALUES ";

		$total = 0;
		foreach($data as $id => $dt){
			if($id > ($ins['mulai'] - 1)){
				$total++;
				$dt_a = $this->db->escape($dt[$a]); $dt_b = $this->db->escape($dt[$b]); $dt_c = $this->db->escape($dt[$c]);
				$dt_d = $this->db->escape($dt[$d]); $dt_e = $this->db->escape($dt[$e]); $dt_f = $this->db->escape(trim($dt[$f]));
				$dt_g = $this->db->escape(trim($dt[$g])); $dt_h = $this->db->escape(trim($dt[$h])); 
				$dt_j = $this->db->escape(trim($dt[$j])); $dt_k = $this->db->escape(trim($dt[$k])); $dt_l = $this->db->escape(trim($dt[$l]));
				$dt_m = $this->db->escape(trim($dt[$m])); $dt_n = $this->db->escape(trim($dt[$n])); $dt_o = $this->db->escape(trim($dt[$o]));
				$dt_p = $this->db->escape(trim($dt[$p])); $dt_q = $this->db->escape(trim($dt[$q])); $dt_r = $this->db->escape(trim($dt[$r]));
				$dt_s = $this->db->escape(trim($dt[$s])); $dt_t = $this->db->escape(trim($dt[$t])); $dt_u = $this->db->escape(trim($dt[$u]));				

				$format_tanggal_bupot = $this->format_tanggal_excel($dt[$u]);
				$dt_bukti_potong = $this->db->escape(trim($format_tanggal_bupot));				
				
				$ins_data_bupot_bulanan .= "('{$dt_email_dikirim}', {$dt_a}, {$dt_b}, {$dt_c}, {$dt_d}, {$dt_e}, {$dt_f},
									{$dt_g}, {$dt_h}, {$dt_j}, {$dt_k},
									{$dt_l}, {$dt_m}, {$dt_n}, {$dt_o}, {$dt_p},
									{$dt_q}, {$dt_r}, {$dt_s}, {$dt_t}, {$dt_bukti_potong},
									'{$var_ttd_stempel}', '{$int_status}', '{$dt_periode}', {$id_import}),";
			}
		}
		$update_import = $this->update_import($id_import, $total);

		$ins_data_bupot_bulanan = rtrim($ins_data_bupot_bulanan, ',').';';
		$this->db->query($ins_data_bupot_bulanan);
		
		if ($this->db->trans_status() === FALSE || $update_import === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return $total;
		}
	}

	function update_import($id_import, $total){
		$upd['total'] = $total;
		$this->db->trans_begin();

		$this->db->where('h_id', $id_import);
		$this->db->update($this->h_import, $upd);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	function format_tanggal_excel($str_date){
		if(is_numeric($str_date)){
			$int_date = $str_date - 2;
			$add_date = date('Y-m-d', strtotime('1900-01-01'. ' + '.$int_date.' days'));
			$strtotime = strtotime($add_date);
			return date('Y-m-d',$strtotime);
		}else{
			return '';
		}
	}
}

class MyReadFilter implements IReadFilter {
    private $startRow = 0;
    private $columns = [];

    public function __construct($startRow, $columns){
        $this->startRow = $startRow;
        $this->columns = $columns;
    }

    public function readCell($column, $row, $worksheetName = ''){
        if ($row >= $this->startRow) {
            if (in_array($column, $this->columns)) {
                return true;
            }
        }
        return false;
    }
}
