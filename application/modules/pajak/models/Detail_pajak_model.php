<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Detail_pajak_model extends MY_Model {

    public function list($id_import, $status_filter = "", $filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$this->db->select("*")
					->from($this->t_potongan_pajak)
					->where("id_import", $id_import);

		if(($status_filter != "")){
			$this->db->where('int_status', $status_filter);
		}
			
		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('var_nik', $filter)
					->or_like('var_nama', $filter)
					->or_like('var_npwp', $filter)
					->or_like('var_email', $filter)
					->group_end();
		}

		switch($order_by){
			case 1 : $order = 'var_nik '; break;
			case 2 : $order = 'var_nama '; break;
			case 3 : $order = 'var_npwp '; break;
			case 4 : $order = 'var_email '; break;
			default: {$order = 'int_potongan_pajak_id '; $sort = 'ASC';} break;

		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($id_import, $status_filter = "", $filter = NULL){
		$this->db->from($this->t_potongan_pajak)
				->where("id_import", $id_import);

		if(($status_filter != "")){
			$this->db->where('int_status', $status_filter);
		}

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('var_nik', $filter)
					->or_like('var_nama', $filter)
					->or_like('var_npwp', $filter)
					->or_like('var_email', $filter)
					->group_end();
		}
		return $this->db->count_all_results();
	}

	public function get_import($h_id){
		return $this->db->select("hi.*, tg.dt_periode")
					->join($this->t_potongan_pajak.' tg', 'hi.`h_id` = tg.`id_import`', 'left')
					->get_where($this->h_import.' hi', ['h_id' => $h_id])->row();
	}

	public function get_bupot($int_potongan_pajak_id){
		return $this->db->select("*")
					->get_where($this->t_potongan_pajak, ['int_potongan_pajak_id' => $int_potongan_pajak_id])->row();
	}

	public function resend($int_potongan_pajak_id){
		$upd['int_status'] = 2;
		$this->db->trans_begin();

		$this->db->where('int_potongan_pajak_id', $int_potongan_pajak_id);
		$this->db->update($this->t_potongan_pajak, $upd);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}
	
	public function resendall($id_import){
		$upd['int_status'] = 2;
		$this->db->trans_begin();

		$this->db->where('id_import', $id_import);
		$this->db->where('int_status = 4');
		$this->db->update($this->t_potongan_pajak, $upd);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}
}