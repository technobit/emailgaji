<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;

class Pot_pajak_model extends MY_Model {


	public function get_last_email($int_type){
		return $this->db->query("SELECT txt_isi_email, var_subjek_email
								 FROM	{$this->h_import}
								 WHERE int_type = {$int_type}
								 ORDER BY h_id DESC LIMIT 1")->row();
	}

    public function list($filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$this->db->select("*, CONCAT(suc.txt_nama_depan, ' ', suc.txt_nama_belakang) as created,
		CONCAT(suu.txt_nama_depan, ' ', suu.txt_nama_belakang) as updated")
					->from($this->h_import." hi")
					->join($this->s_user." suc", "hi.created_by = suc.int_id_user", "left")
					->join($this->s_user." suu", "hi.updated_by = suu.int_id_user", "left")
					->where('hi.int_type', 1);


		if(($this->session->userdata['int_level'] == 2)){
			$this->db->where('hi.int_level', 2);
		}
			
		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('import_date', $filter)
					->or_like('file_name', $filter)
					->or_like('total', $filter)
					->or_like('dt_email_dikirim', $filter)
					->group_end();
		}

		switch($order_by){
			case 1 : $order = 'import_date '; break;
			case 2 : $order = 'file_name '; break;
			case 3 : $order = 'total '; break;
			case 4 : $order = 'dt_email_dikirim '; break;
			default: {$order = 'import_date '; $sort = 'DESC';} break;

		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($filter = NULL){
		$this->db->from($this->h_import)
					->where('int_type', 1);

        if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
            $this->db->group_start()
			->like('import_date', $filter)
			->or_like('file_name', $filter)
			->or_like('total', $filter)
			->or_like('dt_email_dikirim', $filter)
			->group_end();
        }
		return $this->db->count_all_results();
	}

	public function get($h_id){
		return $this->db->select("hi.*, tp.dt_periode")
					->join($this->t_potongan_pajak.' tp', 'hi.`h_id` = tp.`id_import`', 'left')
					->get_where($this->h_import.' hi', ['h_id' => $h_id])->row();
	}

	public function update($h_id, $upd){
		$upd_import['var_subjek_email'] = $upd['var_subjek_email'];
		$upd_import['txt_isi_email'] = $upd['txt_isi_email'];
		$upd_import['dt_email_dikirim'] = $upd['dt_email_dikirim'];
		$upd_import['updated_by'] = $this->session->userdata['user_id'];
		$upd_pot_pajak['dt_periode'] = $upd['dt_periode'];
		$upd_pot_pajak['dt_email_dikirim'] = $upd['dt_email_dikirim'];

		$this->db->trans_begin();

		$this->db->where('h_id', $h_id);
		$this->db->update($this->h_import, $upd_import);

		$upd_data_pot_pajak = $this->upd_data_pot_pajak($h_id, $upd_pot_pajak);

		if ($this->db->trans_status() === FALSE|| $upd_data_pot_pajak === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function upd_data_pot_pajak($id_import, $upd){
		$this->db->trans_begin();

		$this->db->where('id_import', $id_import);
		$this->db->update($this->t_potongan_pajak, $upd);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete($h_id){
		$this->db->trans_begin();
		$this->db->delete($this->h_import,  ['h_id' => $h_id]);

		$del_data_pot_pajak = $this->del_data_pot_pajak($h_id);

		if ($this->db->trans_status() === FALSE || $del_data_pot_pajak === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function del_data_pot_pajak($id_import){
		$this->db->trans_begin();
		$this->db->delete($this->t_potongan_pajak,  ['id_import' => $id_import]);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function import($ins, $file){
		$user 	= $this->session->userdata('username');
		$dt_periode = $ins['dt_periode'];
		$dt_email_dikirim = $ins['dt_email_dikirim'];
		$var_ttd_stempel = $ins['var_ttd_stempel'];
		$created_by = $this->session->userdata['user_id'];
		$int_level = $this->session->userdata['int_level'];

		$int_status = 2;
		$a = 'A'; $b = 'B'; $c = 'C'; $d = 'D'; $e = 'E'; $f = 'F'; $g = 'G'; $h = 'H'; $i = 'I'; $j = 'J';
		$k = 'K'; $l = 'L'; $m = 'M'; $n = 'N'; $o = 'O'; $p = 'P'; $q = 'Q'; $r = 'R'; $s = 'S'; $t = 'T';
		$u = 'U'; $v = 'V'; $w = 'W'; $x = 'X'; $y = 'Y'; $z = 'Z';
		$aa = 'AA'; $ab = 'AB'; $ac = 'AC'; $ad = 'AD'; $ae = 'AE'; $af = 'AF'; $ag = 'AG'; $ah = 'AH'; $ai = 'AI'; $aj = 'AJ';
		$ak = 'AK'; $al = 'AL'; $am = 'AM'; $an = 'AN'; $ao = 'AO'; $ap = 'AP'; $aq = 'AQ'; $ar = 'AR'; $as = 'AS';


		$filterSubset = new MyReadFilter($ins['mulai'],
						[$a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $n, $o, $p, $q, $r, $s, $t, $u, $v, $w, $x, $y, $z,
						$aa, $ab, $ac, $ad, $ae, $af, $ag, $ah, $ai, $aj, $ak, $al, $am, $an, $ao, $ap, $aq, $ar, $as]
					);

		$reader = IOFactory::createReader(ucfirst(ltrim($file['file_ext'],'.')));
		$reader->setReadDataOnly(true);
		$reader->setReadFilter($filterSubset);
		$spreadsheet = $reader->load($file['full_path']);
		$data = $spreadsheet->getActiveSheet()->toArray(null, true, false, true);
		
		$this->db->trans_begin();
		
		$this->db->insert($this->h_import, ['username' => $user,
										  'file_name' => $file['orig_name'],
										  'direktori' => $file['full_path'],
										  'var_subjek_email' => $ins['var_subjek_email'],
										  'txt_isi_email' => $ins['txt_isi_email'],
										  'dt_email_dikirim' => $ins['dt_email_dikirim'],
										  'created_by' => $created_by,
										  'int_level' => $int_level,
										  'int_type' => 1
										  ]);
										  
		$id_import = $this->db->insert_id();
		$ins_data_pot_pajak = "INSERT INTO {$this->t_potongan_pajak} 
						(`var_nik`, `var_nama`, `var_email`, `var_masa_pajak`, `var_tahun_pajak`,
						`dec_pembetulan`, `var_no_bukti_potong`, `var_masa_perolehan_awal`, `var_masa_perolehan_akhir`, `var_npwp_pemotong_jai`,
						`var_nama_pemotong_jai`, `var_npwp`, `var_nik_ktp`, `var_alamat`, `var_jenis_kelamin`,
						`var_status_ptkp`, `int_jumlah_tanggungan`, `var_nama_jabatan`, `var_wp_luar_negeri`, `var_kode_negara`,
						`var_kode_pajak`,
						`dec_jumlah_1`, `dec_jumlah_2`, `dec_jumlah_3`, `dec_jumlah_4`, `dec_jumlah_5`,
						`dec_jumlah_6`, `dec_jumlah_7`, `dec_jumlah_8`, `dec_jumlah_9`, `dec_jumlah_10`,
						`dec_jumlah_11`, `dec_jumlah_12`, `dec_jumlah_13`, `dec_jumlah_14`, `dec_jumlah_15`,
						`dec_jumlah_16`, `dec_jumlah_17`, `dec_jumlah_18`, `dec_jumlah_19`, `dec_jumlah_20`,
						`var_npwp_pemotong`, `var_nama_pemotong`, `dt_bukti_potong`, `dt_email_dikirim`, `dt_periode`, `var_ttd_stempel`,
						`int_status`, `id_import`) VALUES ";

		$total = 0;
		foreach($data as $id => $dt){
			if($id > ($ins['mulai'] - 1)){
				$total++;
				$dt_a = $this->db->escape($dt[$a]); $dt_b = $this->db->escape($dt[$b]); $dt_c = $this->db->escape($dt[$c]);
				$dt_d = $this->db->escape($dt[$d]); $dt_e = $this->db->escape($dt[$e]); $dt_f = $this->db->escape(trim($dt[$f]));
				$dt_g = $this->db->escape(trim($dt[$g])); $dt_h = $this->db->escape(trim($dt[$h])); $dt_i = $this->db->escape(trim($dt[$i]));
				$dt_j = $this->db->escape(trim($dt[$j])); $dt_k = $this->db->escape(trim($dt[$k])); $dt_l = $this->db->escape(trim($dt[$l]));
				$dt_m = $this->db->escape(trim($dt[$m])); $dt_n = $this->db->escape(trim($dt[$n])); $dt_o = $this->db->escape(trim($dt[$o]));
				$dt_p = $this->db->escape(trim($dt[$p])); $dt_q = $this->db->escape(trim($dt[$q])); $dt_r = $this->db->escape(trim($dt[$r]));
				$dt_s = $this->db->escape(trim($dt[$s])); $dt_t = $this->db->escape(trim($dt[$t])); $dt_u = $this->db->escape(trim($dt[$u]));
				$dt_v = $this->db->escape(trim($dt[$v])); $dt_w = $this->db->escape(trim($dt[$w])); $dt_x = $this->db->escape(trim($dt[$x]));
				$dt_y = $this->db->escape(trim($dt[$y])); $dt_z = $this->db->escape(trim($dt[$z]));
				$dt_aa = $this->db->escape(trim($dt[$aa])); $dt_ab = $this->db->escape(trim($dt[$ab])); $dt_ac = $this->db->escape(trim($dt[$ac]));
				$dt_ad = $this->db->escape(trim($dt[$ad])); $dt_ae = $this->db->escape(trim($dt[$ae])); $dt_af = $this->db->escape(trim($dt[$af]));
				$dt_ag = $this->db->escape(trim($dt[$ag])); $dt_ah = $this->db->escape(trim($dt[$ah])); $dt_ai = $this->db->escape(trim($dt[$ai]));
				$dt_aj = $this->db->escape(trim($dt[$aj])); $dt_ak = $this->db->escape(trim($dt[$ak])); $dt_al = $this->db->escape(trim($dt[$al]));
				$dt_am = $this->db->escape(trim($dt[$am])); $dt_an = $this->db->escape(trim($dt[$an])); $dt_ao = $this->db->escape(trim($dt[$ao]));
				$dt_ap = $this->db->escape(trim($dt[$ap])); $dt_aq = $this->db->escape(trim($dt[$aq])); $dt_ar = $this->db->escape(trim($dt[$ar]));

				$format_tanggal_bupot = $this->format_tanggal_excel($dt[$as]);
				$dt_bukti_potong = $this->db->escape(trim($format_tanggal_bupot));				
				
				$ins_data_pot_pajak .= "({$dt_a}, {$dt_b}, {$dt_c}, {$dt_d}, {$dt_e}, {$dt_f}, {$dt_g}, {$dt_h}, {$dt_i}, {$dt_j},
									{$dt_k}, {$dt_l}, {$dt_m},
									{$dt_o}, {$dt_p}, {$dt_q}, {$dt_r}, {$dt_s}, {$dt_t},
									{$dt_u}, {$dt_v}, {$dt_w}, {$dt_x}, {$dt_y}, {$dt_z},
									{$dt_aa}, {$dt_ab}, {$dt_ac}, {$dt_ad}, {$dt_ae}, {$dt_af}, {$dt_ag}, {$dt_ah}, {$dt_ai}, {$dt_aj},
									{$dt_ak}, {$dt_al}, {$dt_am}, {$dt_an}, {$dt_ao}, {$dt_ap}, {$dt_aq}, {$dt_ar}, {$dt_bukti_potong},
									'{$dt_email_dikirim}', '{$dt_periode}', '{$var_ttd_stempel}', {$int_status}, {$id_import}),";

			}
		}
		$update_import = $this->update_import($id_import, $total);

		$ins_data_pot_pajak = rtrim($ins_data_pot_pajak, ',').';';
		$this->db->query($ins_data_pot_pajak);
		
		if ($this->db->trans_status() === FALSE || $update_import === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return $total;
		}
	}

	function update_import($id_import, $total){
		$upd['total'] = $total;
		$this->db->trans_begin();

		$this->db->where('h_id', $id_import);
		$this->db->update($this->h_import, $upd);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	function format_tanggal_excel($str_date){
		if(is_numeric($str_date)){
			$int_date = $str_date - 2;
			$add_date = date('Y-m-d', strtotime('1900-01-01'. ' + '.$int_date.' days'));
			$strtotime = strtotime($add_date);
			return date('Y-m-d',$strtotime);
		}else{
			return '';
		}
	}
}

class MyReadFilter implements IReadFilter {
    private $startRow = 0;
    private $columns = [];

    public function __construct($startRow, $columns){
        $this->startRow = $startRow;
        $this->columns = $columns;
    }

    public function readCell($column, $row, $worksheetName = ''){
        if ($row >= $this->startRow) {
            if (in_array($column, $this->columns)) {
                return true;
            }
        }
        return false;
    }
}
