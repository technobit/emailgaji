<div class="container-fluid">
    <div class="row">
        <section class="col-lg-12">
            <div class="card card-outline">
                <div class="card-header">
                    <h3 class="card-title mt-1">
                        <i class="<?=isset($breadcrumb->icon)? $breadcrumb->icon : 'far fa-circle'?>"></i>
                        Detil Potongan Pajak
                    </h3>
                </div><!-- /.card-header -->
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                        <tbody><tr>
                            <th style="width:15%">Waktu Upload :</th>
                            <td style="width:35%"><?=idn_date($detail->import_date, "j F Y H:i:s")?></td>
                            <th style="width:15%">Periode Pajak :</th>
                            <td style="width:35%"><?=idn_date($detail->dt_periode, "F Y")?></td>
                        </tr>
                        <tr>
                            <th>Nama File :</th>
                            <td><?=$detail->file_name?></td>
                            <th>Jadwal Kirim :</th>
                            <td><?=idn_date($detail->dt_email_dikirim)?></td>
                        </tr>
                        <tr>
                            <th>Total Data :</th>
                            <td><?=$detail->total?> Karyawan</td>
                            <th><a href="<?=$url_back?>" class="btn btn-sm btn-danger tooltips"><i class="fas fa-reply"> </i> Kembali</a></th>
                            <td></td>
                        </tr>
                        <tr>
                        </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </section>
        <section class="col-lg-12">
            <div class="card card-outline">
                <div class="card-header">
                    <h3 class="card-title mt-1">
                        <i class="<?=isset($breadcrumb->icon)? $breadcrumb->icon : 'far fa-circle'?>"></i>
                        Data Penerima Gaji
                    </h3>
                </div><!-- /.card-header -->
                <div class="card-body p-0">
                    <div id="filter" class="form-horizontal filter-date p-2 border-bottom">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group row text-sm mb-0">
                                    <label for="status_filter" class="col-md-4 col-form-label">Status</label>
									<div class="col-md-8">
                                        <select id="status_filter" name="status_filter" class="form-control form-control-sm status_filter select2" style="width: 100%;">
                                            <option value="">- Semua Status - </option>
                                            <option value="2">Proses Pengiriman</option>
                                            <option value="3">Terkirim</option>
                                            <option value="4">Gagal Dikirim</option>
                                        </select>
									</div>
								</div>
							</div>
                            <div class="col-md-8 text-right">
							</div>
						</div>
					</div>
                    <table class="table table-striped table-hover table-full-width" id="table_gaji">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>NIK</th>
                            <th>Nama</th>
                            <th>NPWP</th>
                            <th>Email</th>
                            <th>Tanggal Kirim</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>
<div id="ajax-modal-confirm" class="modal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="50%" aria-hidden="true"></div>
