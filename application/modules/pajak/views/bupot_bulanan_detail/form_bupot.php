<div class="container-fluid">
    <div class="row">
        <section class="col-lg-12">
        <div class="d-print-none mb-3">
            <a href="<?=$url?>" class="btn btn-sm btn-danger tooltips"><i class="fas fa-reply"> </i> Kembali</a>
            <a href="#" onclick="window.print()" class="btn btn-sm btn-primary tooltips"><i class="fas fa-print"> </i> Print</a>
        </div>
        <div class="card card-outline" style="width:220mm; margin:auto">
            <div class="card-body">
                <div id="container" class="d-print-block" style="width:100%;width:210mm; height: 297mm; margin:auto;font-family: Arial, sans-serif;font-size: 14px; border: 1px solid #eee;">

					<div id="kop_bupot" style="border-top: 1px solid #aaa; margin-top: 10px;">
						<div style="height:11px; width:25px; margin-top: -4px; float: left;">
							<img src="<?=base_url()?>assets/images/black.png" width="100%" height="100%">
						</div>
						<div style="height:11px; width:25px; margin-top: -4px; float: right;">
							<img src="<?=base_url()?>assets/images/black.png" width="100%" height="100%">
						</div>

						<table width="100%" style="margin-top:15px;">
							<tr>
								<td width="25%" style="border-bottom:none;border-left: none;border-top: none; text-align: center;">
									<img height="75px" src="http://localhost/emailjai/assets/images/logo-djp.png">
								</td>
								<td width="45%" style="text-align: center; border-top: none;">
									<b>BUKTI PEMOTONGAN PAJAK<br>
									PENGHASILAN PASAL 21 BULANAN<br></b>
								</td>
								<td width="25%" style="border-top:none; border-left: none; border-right: none;">
									<div style="display: block;margin-right: -5px; margin-top: 0px;">
										<span style="height:10px; width:15px; border:1px solid #000;float: right; margin-left: 3px;"></span>
										<span style="height:11px; width:16px; background: #000;float: right; margin-left: 3px;"></span>
										<span style="height:10px; width:15px; border:1px solid #000;float: right; margin-left: 3px;"></span>
										<span style="height:11px; width:16px; background: #000;float: right; margin-left: 3px;"></span>
									</div>
									<div style="text-align: right;font-size: 12; display: block;"><br><b>FORMULIR 1721 - VII</b><br></div>
									<div style="text-align: left;font-size: 8px; margin-top:10px">Lembar ke-1 : untuk Penerima Penghasilan</div>
								</td>
							</tr>
							<tr>
								<td style="font-size:11px; text-align: center; border-left:none; border-top: none;">
									<b>KEMENTRIAN KEUANGAN RI<br>
									DIREKTORAT JENDRAL PAJAK</b>
								</td>
								<td colspan="2" style="font-size: 12px;border-right:none">
									<b>Nomor : </b>
									<span style="width: 200px !important; border-bottom: 1px solid; display: inline-block;"> <?=isset($data->var_no_bukti_potong)? $data->var_no_bukti_potong : ''?> </span>
									<span style="width: 50px !important; display: inline-block;"></span>
									<b>Masa Pajak - Tahun Pajak : </b>
									<span style="width: 80px !important; border-bottom: 1px solid; display: inline-block;"><?=isset($data->var_masa_pajak)? $data->var_masa_pajak : ''?> -  <?=isset($data->var_tahun_pajak)? $data->var_tahun_pajak : ''?></span>
								</td>
							</tr>
						</table>
					</div>

					<div id="penerima_bupot" style="padding:5px; margin-top:5px">
						<div><b>A. IDENTITAS PENERIMA PENGHASILAN YANG DIPOTONG</b></div>
						<table width="100%" style="border: 1px solid; font-size: 12px;">
							<tr>
								<td class="no-border" width="15%">1. NPWP</td>
								<td class="no-border" width="35%">: 
									<span style="width: calc( 100% - 30px) !important; border-bottom: 1px solid; display: inline-block;">
										<?=isset($data->var_npwp)? substr($data->var_npwp, 0, 2) : ''?>.
										<?=isset($data->var_npwp)? substr($data->var_npwp, 2, 3) : ''?>.
										<?=isset($data->var_npwp)? substr($data->var_npwp, 5, 3) : ''?>.
										<?=isset($data->var_npwp)? substr($data->var_npwp, 8, 1) : ''?>-
										<?=isset($data->var_npwp)? substr($data->var_npwp, 9, 3) : ''?>.
										<?=isset($data->var_npwp)? substr($data->var_npwp, 12, 3) : ''?>
									</span>
								</td>
								<td class="no-border">
									2. NIK : 
									<span style="width: calc( 100% - 67px) !important; border-bottom: 1px solid; display: inline-block;"><?=isset($data->var_nik_ktp)? $data->var_nik_ktp : ''?></span>
								</td>
							</tr>
							<tr>
								<td class="no-border">3. NAMA</td>
								<td class="no-border" colspan="2">: 
									<span style="width: calc( 100% - 30px) !important; border-bottom: 1px solid; display: inline-block;"><?=isset($data->var_nama)? $data->var_nama : ''?></span></td>
							</tr>
							<tr>
								<td class="no-border">4. ALAMAT</td>
								<td class="no-border" colspan="2">: 
									<span style="width: calc( 100% - 30px) !important; border-bottom: 1px solid; display: inline-block;"><?=isset($data->var_alamat)? $data->var_alamat : ''?></span></td>
							</tr>
						</table>
					</div>

					<div id="rincian_bupot" style="padding:5px; margin-top:5px">
						<div><b>B. PPh PASAL 21 DAN/ATAU PASAL 26 YANG DIPOTONG</b></div>
						<table width="100%" style="border: 1px solid; font-size: 11px; padding: 2px;">
							<tr>
								<td class="pd-3" style="text-align: center;">KODE OBJEK PAJAK</td>
								<td class="pd-3" style="text-align: center;">JUMLAH<br>PENGHASILAN BRUTO<br>(Rp)</td>
								<td class="pd-3" style="text-align: center;">DASAR PENGENAAN<br>PAJAK<br>(Rp)</td>
								<td class="pd-3" style="text-align: center;">TARIF LEBIH<br>TINGGI 20%<br>(TIDAK BER-<br>NPWP)</td>
								<td class="pd-3" style="text-align: center;">TARIF<br>(%)</td>
								<td class="pd-3" style="text-align: center;">PPh DIPOTONG<br>(Rp)</td>
							</tr>
							<tr>
								<td class="pd-3" style="background: #ccc; text-align: center;">(1)</td>
								<td class="pd-3" style="background: #ccc; text-align: center;">(2)</td>
								<td class="pd-3" style="background: #ccc; text-align: center;">(3)</td>
								<td class="pd-3" style="background: #ccc; text-align: center;">(4)</td>
								<td class="pd-3" style="background: #ccc; text-align: center;">(5)</td>
								<td class="pd-3" style="background: #ccc; text-align: center;">(6)</td>
							</tr>
							<tr>
								<td class="pd-3" style="text-align: center;">
									<?=isset($data->var_kode_pajak)? $data->var_kode_pajak : ''?>
								</td>
								<td class="pd-3" style="text-align: right;">
									<?=isset($data->dec_jumlah_penghasilan_bruto)? number_format($data->dec_jumlah_penghasilan_bruto,0, ',', '.' ) : '-'?>
								</td>
								<td class="pd-3" style="text-align: right;">
									<?=isset($data->dec_dasar_pengenaan_pajak)? number_format($data->dec_dasar_pengenaan_pajak,0, ',', '.' ) : '-'?>
								</td>
								<td class="pd-3" style="text-align: center;">
									<div style="width: 22px !important; height: 22px; display: inline-block; border: 1px solid; text-align: center; margin:1px 0 1px 0; font-size: 14px;"><?=($data->var_tarif_tidak_ber_npwp == '') ? '' : 'X';?></div>
								</td>
								<td class="pd-3" style="text-align: center;">
									<?=isset($data->dec_tarif)? number_format($data->dec_tarif,2, ',', '.' ) : '-'?>
								</td>
								<td class="pd-3" style="text-align: right;">
									<?=isset($data->dec_pph_dipotong)? number_format($data->dec_pph_dipotong,0, ',', '.' ) : '-'?>
								</td>
							</tr>
						</table>
					</div>
					<div id="referensi_fasilitas" style="padding:5px; margin-top:5px">
						<div><b>C. NOMOR DOKUMEN REFERENSI FASILITAS :</b></div>
						<table width="100%" style="border-bottom: 1px solid; font-size: 11px; padding: 5px 0;">
							<tr>
								<td class="no-border" width="100%"><?=isset($data->var_no_dokumen_ref)? $data->var_no_dokumen_ref : '-'?></td>
							</tr>
						</table>
					</div>
					<div id="pemotong_bupot" style="padding:5px; margin-top:5px">
						<div><b>D. IDENTITAS PEMOTONG</b></div>
						<table width="100%" style="border: 1px solid; font-size: 11px; padding: 5px 0;">
							<tr>
								<td class="no-border" width="10%">1. NPWP</td>
								<td class="no-border" width="45%">:
									<span style="width: calc( 100% - 30px) !important; border-bottom: 1px solid; display: inline-block;">
										<?=isset($data->var_npwp_pemotong)? substr($data->var_npwp_pemotong, 0, 2) : ''?>.
										<?=isset($data->var_npwp_pemotong)? substr($data->var_npwp_pemotong, 2, 3) : ''?>.
										<?=isset($data->var_npwp_pemotong)? substr($data->var_npwp_pemotong, 5, 3) : ''?>.
										<?=isset($data->var_npwp_pemotong)? substr($data->var_npwp_pemotong, 8, 1) : ''?>-
										<?=isset($data->var_npwp_pemotong)? substr($data->var_npwp_pemotong, 9, 3) : ''?>.
										<?=isset($data->var_npwp_pemotong)? substr($data->var_npwp_pemotong, 12, 3) : ''?>
									</span>
								</td>
								<td class="no-border" width="25%">4. TANGGAL & TANDA TANGAN</td>
								<td class="no-border" width="20%" rowspan="3">
									<div style="width: 100%; height: 100px; border: 1px solid; text-align: center;">
										<img style="max-width: 90%; max-height:90%; margin:auto" src="<?=base_url()?><?=isset($data->var_ttd_stempel)? $data->var_ttd_stempel : '/assets/images/ttd-stempel-jai.jpg'?>">
									</div>
								</td>
							</tr>
							<tr>
								<td class="no-border" width="10%" style="padding:5px;">2. NAMA</td>
								<td class="no-border" width="45%" style="padding:5px;">: 
									<span style="width: calc( 100% - 30px) !important; border-bottom: 1px solid; display: inline-block;"><?=isset($data->var_nama_pemotong)? $data->var_nama_pemotong : ''?></span></td>
								<td class="no-border" width="25%" style="padding:5px;">
								</td>
							</tr>
							<tr>
								<td class="no-border" width="10%" style="padding:5px;">3. NAMA<br><span style="margin-left:10px">PENANDATANGAN</span></td>
								<td class="no-border" width="45%" style="padding:5px;">: 
									<span style="width: calc( 100% - 30px) !important; border-bottom: 1px solid; display: inline-block;"><?=isset($data->var_nama_penandatangan)? $data->var_nama_penandatangan : ''?></span></td>
								<td class="no-border" width="25%" style="padding:5px;">
									<span style="width: calc( 100% - 30px) !important; border-bottom: 1px solid; display: inline-block;">
									<?php
										if(isset($data->dt_tgl_bukti_potong)){
											$tanggal = strtotime($data->dt_tgl_bukti_potong);
											$format_tanggal = date("d-m-Y", $tanggal);
											echo $format_tanggal;
										}else{
											echo '-';
										}
									?>
									</span><br>
								</td>
							</tr>

						</table>
					</div>
					<div id="info_bupot" style="padding:5px; margin-top:5px">
						<table width="100%" style="border: 1px solid; font-size: 11px; padding: 5px 0;">
							<tr style="border-bottom: 1px solid;">
								<td class="no-border" colspan="3" style="background: #ccc; text-align: center;"><b>KODE OBJEK PAJAK PENGHASILAN PASAL 21 BULANAN</b></td>
							</tr>
							<tr>
								<td class="no-border" width="3%">1.</td>
								<td class="no-border" width="12%">21-100-01</td>
								<td class="no-border" width="85%">Penghasilan yang diterima oleh Pegawai Tetap</td>
							</tr>
							<tr>
								<td class="no-border" width="3%">2.</td>
								<td class="no-border" width="12%">21-100-02</td>
								<td class="no-border" width="85%">Uang terkait Pensiun yang Diterima oleh Pensiunan secara Berkala</td>
							</tr>
						</table>

						<div style="height:12px; width:23px; margin-top: 385px; float: left;">
							<img src="<?=base_url()?>assets/images/black.png" width="100%" height="100%">
						</div>
						<div style="height:12px; width:23px; margin-top: 385px; float: right;">
							<img src="<?=base_url()?>assets/images/black.png" width="100%" height="100%">
						</div>
					</div>
                </div>
            </div>
        </div>

        <div class="d-print-none text-right" style="margin:15px 0;">
            <a href="<?=$url?>" class="btn btn-sm btn-danger tooltips"><i class="fas fa-reply"> </i> Kembali</a>
            <a href="#" onclick="window.print()" class="btn btn-sm btn-primary tooltips"><i class="fas fa-print"> </i> Print</a>
        </div>
        </section>
    </div>
</div>