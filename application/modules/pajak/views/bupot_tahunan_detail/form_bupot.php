<div class="container-fluid">
    <div class="row">
        <section class="col-lg-12">
        <div class="d-print-none mb-3">
            <a href="<?=$url?>" class="btn btn-sm btn-danger tooltips"><i class="fas fa-reply"> </i> Kembali</a>
            <a href="#" onclick="window.print()" class="btn btn-sm btn-primary tooltips"><i class="fas fa-print"> </i> Print</a>
        </div>
        <div class="card card-outline" style="width:220mm; margin:auto">
            <div class="card-body">
                <div id="container" class="d-print-block" style="width:100%;width:210mm; margin:auto;font-family: Arial, sans-serif;font-size:11px; ">

					<div id="kop_bupot" style="border-top: 1px solid #aaa; margin-top: 10px;">
						<div style="height:11px; width:25px; margin-top: -4px; float: left;">
							<img src="<?=base_url()?>assets/images/black.png" width="100%" height="100%">
						</div>
						<div style="height:11px; width:25px; margin-top: -4px; float: right;">
							<img src="<?=base_url()?>assets/images/black.png" width="100%" height="100%">
						</div>
						<table width="100%" style="margin-top: 0px;">
							<tr>
								<td width="25%" style="border-bottom:none;border-left: none;border-top: none; text-align: center;">
									<img height="75px" src="<?=base_url()?>assets/images/logo-djp.png">
								</td>
								<td width="45%" style="text-align: center; border-top: none;">
									BUKTI PEMOTONGAN PAJAK PENGHASILAN PASAL 21<br>
									BAGI PEGAWAI TETAP ATAU PENSIUNAN YANG<br>
									MENERIMA UANG TERKAIT PENSIUN SECARA BERKALA<br>
								</td>
								<td width="5%" style="border-right:none;border-top: none;">
								</td>
								<td rowspan="2" width="25%" style="border-top:none; border-left: none; border-right: none;">
									<div style="display: block; margin-top:0px; margin-right: -5px;">
										<div style="height:12px; width:12px; border:1px solid #000; float: right; margin-top: 6px; margin-left: 3px;">
											<img src="<?=base_url()?>assets/images/black.png" style="margin-top: -9px; width: 100%; height: 100%;">
										</div>
										<div style="height:12px; width:12px; border:1px solid #000; float: right; margin-top: 6px; margin-left: 3px;"></div>
										<div style="height:12px; width:12px; border:1px solid #000; float: right; margin-top: 6px; margin-left: 3px;">
											<img src="<?=base_url()?>assets/images/black.png" style="margin-top: -9px; width: 100%; height: 100%;">
										</div>
										<div style="height:12px; width:12px; border:1px solid #000; float: right; margin-top: 6px; margin-left: 3px;"></div>
									</div>
									<div style="text-align: right;font-size: 12px; display: block;"><br><b>FORMULIR 1721 - A1</b></div>
									<div style="text-align: left;font-size: 8px;">Lembar ke-1 : untuk Penerima Penghasilan</div>
									<div style="text-align: left;font-size: 8px;">Lembar ke-2 : untuk Pemotong</div><br>
									<div style="text-align: center;font-size: 11px;margin-bottom:4px">
										MASA PEROLEHAN PENGHASILAN<br>[mm-mm]<br>
										<span style="font-size: 8px;color:#428df5;">H.02</span>
										<span style="width: 50px !important; border-bottom: 1px solid; display: inline-block;">
											<?=isset($data->var_masa_perolehan_awal)? $data->var_masa_perolehan_awal : ''?>
										</span> - 
										<span style="width: 50px !important; border-bottom: 1px solid; display: inline-block;">
											<?=isset($data->var_masa_perolehan_akhir)? $data->var_masa_perolehan_akhir : ''?>
										</span><br>
									</div>
								</td>
							</tr>
							<tr>
								<td style="font-size:12px; text-align: center; border-left:none; border-top: none;">
									KEMENTRIAN KEUANGAN RI<br>
									DIREKTORAT JENDERAL PAJAK
								</td>
								<td colspan="2" style="font-size: 11px;">
									<br>NOMOR : <span style="font-size: 8px;color:#428df5;">H.01</span>
									<span style="width: 250px !important; border-bottom: 1px solid; display: inline-block; margin-bottom: 5px;">
										<?=isset($data->var_no_bukti_potong)? $data->var_no_bukti_potong : ''?>
									</span>
								</td>
							</tr>
							<tr style="text-align: left;font-size: 11px;">
								<td style="border-right:none; border-bottom:none;">
									<br>NPWP PEMOTONG
								</td>
								<td colspan="3" style="border-left:none; border-bottom:none;">
									: <span style="font-size: 8px;color:#428df5;">H.03</span>
									<span style="width: 100px !important; border-bottom: 1px solid; display: inline-block;">
										<?=isset($data->var_npwp_pemotong_jai)? substr($data->var_npwp_pemotong_jai, 0, 2) : ''?>.
										<?=isset($data->var_npwp_pemotong_jai)? substr($data->var_npwp_pemotong_jai, 2, 3) : ''?>.
										<?=isset($data->var_npwp_pemotong_jai)? substr($data->var_npwp_pemotong_jai, 5, 3) : ''?>.
										<?=isset($data->var_npwp_pemotong_jai)? substr($data->var_npwp_pemotong_jai, 8, 1) : ''?>
									</span>-
									<span style="width: 50px !important; border-bottom: 1px solid; display: inline-block;">
										<?=isset($data->var_npwp_pemotong_jai)? substr($data->var_npwp_pemotong_jai, 9, 3) : ''?>
									</span>.
									<span style="width: 50px !important; border-bottom: 1px solid; display: inline-block;">
										<?=isset($data->var_npwp_pemotong_jai)? substr($data->var_npwp_pemotong_jai, 12, 3) : ''?>
									</span>
								</td>
							</tr>
							<tr style="text-align: left;font-size: 11px;">
								<td style="border-right:none; border-top:none;">
									NAMA PEMOTONG
								</td>
								<td colspan="3" style="border-left:none; border-top:none;">
									: <span style="font-size: 8px;color:#428df5;">H.04</span>
									<span style="width: calc( 100% - 40px) !important; margin-bottom:1px; border-bottom: 1px solid; display: inline-block;">
										<?=isset($data->var_nama_pemotong_jai)? $data->var_nama_pemotong_jai : ''?>
									</span>
								</td>
							</tr>
						</table>
					</div>

					<div id="penerima_bupot" style="padding:5px; margin-top:5px">
						<div>A. IDENTITAS PENERIMA PENGHASILAN YANG DIPOTONG</div>
						<table width="100%" style="border: 1px solid; font-size: 12px;">
							<tr>
								<td class="no-border" width="15%">1. NPWP</td>
								<td class="no-border" width="35%">: <span style="font-size: 8px;color:#428df5;">A.01</span>
									<span style="width: 100px !important; border-bottom: 1px solid; display: inline-block;">
										<?=isset($data->var_npwp)? substr($data->var_npwp, 0, 2) : ''?>.
										<?=isset($data->var_npwp)? substr($data->var_npwp, 2, 3) : ''?>.
										<?=isset($data->var_npwp)? substr($data->var_npwp, 5, 3) : ''?>.
										<?=isset($data->var_npwp)? substr($data->var_npwp, 8, 1) : ''?>
									</span>
									<span style="width: 50px !important; border-bottom: 1px solid; display: inline-block;">-
										<?=isset($data->var_npwp)? substr($data->var_npwp, 9, 3) : ''?>
									</span>.
									<span style="width: 50px !important; border-bottom: 1px solid; display: inline-block;">
										<?=isset($data->var_npwp)? substr($data->var_npwp, 12, 3) : ''?>
									</span>
								</td>
								<td class="no-border" width="50%">6. STATUS / JUMLAH TANGGUNGAN KELUARGA UNTUK PTKP</td>
							</tr>
							<tr>
								<td class="no-border">2. NIK</td>
								<td class="no-border">: 
									<span style="font-size: 8px;color:#428df5;">A.02</span>
									<span style="width: calc( 100% - 30px) !important; border-bottom: 1px solid; display: inline-block;">
										<?=isset($data->var_nik_ktp)? $data->var_nik_ktp : ''?>
									</span>
								</td>
								<td class="no-border">
									<?php
										if($data->var_status_ptkp == 'K'){
											$tanggungan_k = $data->int_jumlah_tanggungan;
										}else if($data->var_status_ptkp == 'TK'){
											$tanggungan_tk = $data->int_jumlah_tanggungan;
										}else if($data->var_status_ptkp == 'HB'){
											$tanggungan_hb = $data->int_jumlah_tanggungan;
										}
									?>
									<span style="margin-left:15px">K / </span>
									<span style="width: 50px !important; border-bottom: 1px solid; display: inline-block;">
										<?=isset($tanggungan_k)? $tanggungan_k : ''?>
									</span>
									<span style="font-size: 8px;color:#428df5;">A.07</span>
									<span style="margin-left:10px">TK / </span>
									<span style="width: 50px !important; border-bottom: 1px solid; display: inline-block;">
										<?=isset($tanggungan_tk)? $tanggungan_tk : ''?>	
									</span>
									<span style="font-size: 8px;color:#428df5;">A.08</span>
									<span style="margin-left:10px">HB / </span>
									<span style="width: 50px !important; border-bottom: 1px solid; display: inline-block;">
										<?=isset($tanggungan_hb)? $tanggungan_hb : ''?>
									</span>
									<span style="font-size: 8px;color:#428df5;">A.09</span>
								</td>
							</tr>
							<tr>
								<td class="no-border">3. NAMA</td>
								<td class="no-border">: <span style="font-size: 8px;color:#428df5;">A.03</span>
									<span style="width: calc( 100% - 30px) !important; border-bottom: 1px solid; display: inline-block;">	
										<?=isset($data->var_nama)? $data->var_nama : ''?>
									</span>
								</td>
								<td class="no-border">
									7. NAMA JABATAN : 
									<span style="font-size: 8px;color:#428df5;">A.10</span>
									<span style="width: 200px !important; border-bottom: 1px solid; display: inline-block;">
										<?=isset($data->var_nama_jabatan)? $data->var_nama_jabatan : ''?>
									</span>
								</td>
							</tr>
							<tr>
								<td class="no-border" rowspan="2">4. ALAMAT</td>
								<td class="no-border" rowspan="2">: <span style="font-size: 8px;color:#428df5;">A.04</span>
									<span style="width: calc( 100% - 30px) !important; border-bottom: 1px solid; display: inline-block;">
										<?=isset($data->var_alamat)? $data->var_alamat : ''?>
									</span>
								</td>
								<td class="no-border">
									8. KARYAWAN ASING : 
									<span style="font-size: 8px;color:#428df5;">A.11</span>
									<div style="width: 20px; height:20px; display: inline-block; border: 1px solid; text-align: center;">
										<?=($data->var_wp_luar_negeri == 'Y') ? 'X' : '.';?> 
									</div>
									<span style="display: inline-block;">YA</span>
								</td>
							</tr>
							<tr>
							<td class="no-border">
									9. KODE NEGARA DOMISILI : 
									<span style="font-size: 8px;color:#428df5;">A.12</span>
									<span style="width: 160px !important; border-bottom: 1px solid; display: inline-block;">
										<?=isset($data->var_kode_negara)? $data->var_kode_negara : ''?>
									</span>
								</td>
							</tr>
							<tr>
								<td class="no-border">5. JENIS KELAMIN</td>
								<td class="no-border">:
									<span style="font-size: 8px;color:#428df5;">A.05</span>
									<div style="width:20px; height:20px; display: inline-block; border: 1px solid; text-align: center;">
										<?=($data->var_jenis_kelamin == 'M') ? 'X' : '.';?>
									</div>
									<span style="width: 80px !important; display: inline-block;">LAKI-LAKI</span>
									<span style="font-size: 8px;color:#428df5;">A.06</span>
									<div style="width: 20px; height:20px; display: inline-block; border: 1px solid; text-align: center;">
										<?=($data->var_jenis_kelamin == 'F') ? 'X' : '.';?>	
									</div>
									<span style="width: 80px !important; display: inline-block;">PEREMPUAN</span>
								</td>
							</tr>
						</table>
					</div>

					<div id="rincian_bupot" style="padding:5px; margin-top:5px">
						<div>B. RINCIAN PENGHASILAN DAN PERHITUNGAN PPh PASAL 21</div>
						<table width="100%" style="border: 1px solid; font-size: 11px; padding: 2px;">
							<tr>
								<td class="pd-2" colspan="2" style="text-align: center;">URAIAN</td>
								<td class="pd-2" width="120px" style="text-align: center;">JUMLAH</td>
							</tr>
							<tr>
								<td class="pd-2" colspan="2" >KODE OBJEK PAJAK
									<div style="width: 20px !important; display: inline-block; border: 1px solid; text-align: center; margin-left: 25px;">
										<?=($data->var_kode_pajak == '21-100-01') ? 'X' : '.';?>
									</div>
									<span>21-100-01</span>
									<div style="width: 20px !important; display: inline-block; border: 1px solid; text-align: center; margin-left: 25px;">
										<?=($data->var_kode_pajak == '21-100-02') ? 'X' : '.';?>
									</div>
									<span>21-100-02</span>
								</td>
								<td class="pd-2">
									<img src="<?=base_url()?>assets/images/grey.png" style="margin: -3px -3px; width: 119px; height: 25px;">
								</td>
							</tr>
							<tr>
								<td class="pd-2" colspan="2" >PENGHASILAN BRUTO :</td>
								<td class="pd-2">
									<img src="<?=base_url()?>assets/images/grey.png" style="margin: -3px -3px; width: 119px; height: 25px;">
								</td>
							</tr>
							<tr>
								<td class="pd-2" width="20px">1.</td>
								<td class="pd-2">GAJI/PENSIUN ATAU THT/JHT</td>
								<td class="pd-2" style="text-align: right;">
									<?=isset($data->dec_jumlah_1)? number_format($data->dec_jumlah_1,0, ',', '.' ) : '-'?>
								</td>
							</tr>
							<tr>
								<td class="pd-2" width="20px">2.</td>
								<td class="pd-2">TUNJANGAN PPh</td>
								<td class="pd-2" style="text-align: right;">
									<?=isset($data->dec_jumlah_2)? number_format($data->dec_jumlah_2, 0, ',', '.' ) : '-'?>
								</td>
							</tr>
							<tr>
								<td class="pd-2" width="20px">3.</td>
								<td class="pd-2">TUNJANGAN LAINNYA, UANG LEMBUR DAN SEBAGAINYA</td>
								<td class="pd-2" style="text-align: right;">
									<?=isset($data->dec_jumlah_3)? number_format($data->dec_jumlah_3, 0, ',', '.' ) : '-'?>
								</td>
							</tr>
							<tr>
								<td class="pd-2" width="20px">4.</td>
								<td class="pd-2">HONORARIUM DAN IMBALAN LAIN SEJENISNYA</td>
								<td class="pd-2" style="text-align: right;">
									<?=isset($data->dec_jumlah_4)? number_format($data->dec_jumlah_4, 0, ',', '.' ) : '-'?>
								</td>
							</tr>
							<tr>
								<td class="pd-2" width="20px">5.</td>
								<td class="pd-2">PREMI ASURANSI YANG DIBAYAR PEMBERI KERJA</td>
								<td class="pd-2" style="text-align: right;">
									<?=isset($data->dec_jumlah_5)? number_format($data->dec_jumlah_5, 0, ',', '.' ) : '-'?>
								</td>
							</tr>
							<tr>
								<td class="pd-2" width="20px">6.</td>
								<td class="pd-2">PENERIMAAN DALAM BENTUK NATURA DAN KENIKMATAN LAINNYA YANG DIKENAKAN PEMOTONGAN PPh PASAL 21</td>
								<td class="pd-2" style="text-align: right;">
									<?=isset($data->dec_jumlah_6)? number_format($data->dec_jumlah_6, 0, ',', '.' ) : '-'?>
								</td>
							</tr>
							<tr>
								<td class="pd-2" width="20px">7.</td>
								<td class="pd-2">TANTIEM, BONUS, GRATIFIKASI, JASA PRODUKSI, DAN THR</td>
								<td class="pd-2" style="text-align: right;">
									<?=isset($data->dec_jumlah_7)? number_format($data->dec_jumlah_7, 0, ',', '.' ) : '-'?>
								</td>
							</tr>
							<tr>
								<td class="pd-2" width="20px">8.</td>
								<td class="pd-2">JUMLAH PENGHASILAN BRUTO (1 S.D. 7)</td>
								<td class="pd-2" style="text-align: right;">
									<?=isset($data->dec_jumlah_8)? number_format($data->dec_jumlah_8, 0, ',', '.' ) : '-'?>
								</td>
							</tr>
							<tr>
								<td class="pd-2" colspan="2" >PENGURANGAN :</td>
								<td class="pd-2">
									<img src="<?=base_url()?>assets/images/grey.png" style="margin: -3px -3px; width: 119px; height: 25px;">
								</td>
							</tr>
							<tr>
								<td class="pd-2" width="20px">9.</td>
								<td class="pd-2">BIAYA JABATAN/BIAYA PENSIUN</td>
								<td class="pd-2" style="text-align: right;">
									<?=isset($data->dec_jumlah_9)? number_format($data->dec_jumlah_9, 0, ',', '.' ) : '-'?>
								</td>
							</tr>
							<tr>
								<td class="pd-2" width="20px">10.</td>
								<td class="pd-2">IURAN PENSIUN ATAU IURAN THT/JHT</td>
								<td class="pd-2" style="text-align: right;">
									<?=isset($data->dec_jumlah_10)? number_format($data->dec_jumlah_10, 0, ',', '.' ) : '-'?>
								</td>
							</tr>
							<tr>
								<td class="pd-2" width="20px">11.</td>
								<td class="pd-2">ZAKAT/SUMBANGAN KEAGAMAAN YANG BERSIFAT WAJIB YANG DIBAYARKAN MELALUI PEMBERI KERJA</td>
								<td class="pd-2" style="text-align: right;">
								<?=isset($data->dec_jumlah_11)? number_format($data->dec_jumlah_11, 0, ',', '.' ) : '-'?></td>
							</tr>
							<tr>
								<td class="pd-2" width="20px">12.</td>
								<td class="pd-2">JUMLAH PENGURANGAN (9 S.D. 11)</td>
								<td class="pd-2" style="text-align: right;">
									<?=isset($data->dec_jumlah_11)? number_format($data->dec_jumlah_12, 0, ',', '.' ) : '-'?>
								</td>
							</tr>
							<tr>
								<td class="pd-2" colspan="2" >PERHITUNGAN PPh PASAL 21 :</td>
								<td class="pd-2">
									<img src="<?=base_url()?>assets/images/grey.png" style="margin: -3px -3px; width: 119px; height: 25px;">
								</td>
							</tr>
							<tr>
								<td class="pd-2" width="20px">13.</td>
								<td class="pd-2">JUMLAH PENGHASILAN NETO (8 - 12)</td>
								<td class="pd-2" style="text-align: right;">
									<?=isset($data->dec_jumlah_12)? number_format($data->dec_jumlah_13, 0, ',', '.' ) : '-'?>
								</td>
							</tr>
							<tr>
								<td class="pd-2" width="20px">14.</td>
								<td class="pd-2">PENGHASILAN NETO MASA SEBELUMNYA</td>
								<td class="pd-2" style="text-align: right;">
									<?=isset($data->dec_jumlah_13)? number_format($data->dec_jumlah_14, 0, ',', '.' ) : '-'?>
								</td>
							</tr>
							<tr>
								<td class="pd-2" width="20px">15.</td>
								<td class="pd-2">JUMLAH PENGURANGAN NETO UNTUK PERHITUNGAN PPh PASAL 21 (SETAHUN/DISETAHUNKAN)</td>
								<td class="pd-2" style="text-align: right;">
									<?=isset($data->dec_jumlah_14)? number_format($data->dec_jumlah_15, 0, ',', '.' ) : '-'?>
								</td>
							</tr>
							<tr>
								<td class="pd-2" width="20px">16.</td>
								<td class="pd-2">PENGHASILAN TIDAK KENA PAJAK (PTKP)</td>
								<td class="pd-2" style="text-align: right;">
									<?=isset($data->dec_jumlah_15)? number_format($data->dec_jumlah_16, 0, ',', '.' ) : '-'?>
								</td>
							</tr>
							<tr>
								<td class="pd-2" width="20px">17.</td>
								<td class="pd-2">PENGHASILAN KENA PAJAK SETAHUN/DISETAHUNKAN (15 - 16)</td>
								<td class="pd-2" style="text-align: right;">
									<?=isset($data->dec_jumlah_16)? number_format($data->dec_jumlah_17, 0, ',', '.' ) : '-'?>
								</td>
							</tr>
							<tr>
								<td class="pd-2" width="20px">18.</td>
								<td class="pd-2">PPh PASAL 21 ATAS PENGHASILAN KENA PAJAK SETAHUN/DISETAHUNKAN</td>
								<td class="pd-2" style="text-align: right;">
									<?=isset($data->dec_jumlah_17)? number_format($data->dec_jumlah_18, 0, ',', '.' ) : '-'?>
								</td>
							</tr>
							<tr>
								<td class="pd-2" width="20px">19.</td>
								<td class="pd-2">PPh PASAL 21 YANG TELAH DIPOTONG MASA SEBELUMNYA</td>
								<td class="pd-2" style="text-align: right;">
									<?=isset($data->dec_jumlah_18)? number_format($data->dec_jumlah_19, 0, ',', '.' ) : '-'?>
								</td>
							</tr>
							<tr>
								<td class="pd-2" width="20px">20.</td>
								<td class="pd-2">PPh PASAL 21 YANG DITANGGUNG PEMERINTAH (DTP) YANG TELAH DIPOTONG MASA PAJAK SEBELUMNYA</td>
								<td class="pd-2" style="text-align: right;">
									<?=isset($data->dec_jumlah_19)? number_format($data->dec_jumlah_20, 0, ',', '.' ) : '-'?>
								</td>
							</tr>
							<tr>
								<td class="pd-2" width="20px">21.</td>
								<td class="pd-2">PPh PASAL 21 TERUTANG (18 - 19 - 20)</td>
								<td class="pd-2" style="text-align: right;">
									<?=isset($data->dec_jumlah_19)? number_format($data->dec_jumlah_21, 0, ',', '.' ) : '-'?>
								</td>
							</tr>
							<tr>
								<td class="pd-2" width="20px">22.</td>
								<td class="pd-2">PPh PASAL 21 DAN PPh PASAL 26 YANG TELAH DIPOTONG DAN DILUNASI PADA SELAIN MASA PAJAK TERAKHIR</td>
								<td class="pd-2" style="text-align: right;">
									<?=isset($data->dec_jumlah_19)? number_format($data->dec_jumlah_22, 0, ',', '.' ) : '-'?>
								</td>
							</tr>
							<tr>
								<td class="pd-2" width="20px"></td>
								<td class="pd-2">22a. PPh PASAL 21 DIPOTONG</td>
								<td class="pd-2" style="text-align: right;">
									<?=isset($data->dec_jumlah_19)? number_format($data->dec_jumlah_22a, 0, ',', '.' ) : '-'?>
								</td>
							</tr>
							<tr>
								<td class="pd-2" width="20px"></td>
								<td class="pd-2">22b. PPh PASAL 21 DITANGGUNG PEMERINTAH (DTP)</td>
								<td class="pd-2" style="text-align: right;">
									<?=isset($data->dec_jumlah_19)? number_format($data->dec_jumlah_22b, 0, ',', '.' ) : '-'?>
								</td>
							</tr>
							<tr>
								<td class="pd-2" width="20px">23.</td>
								<td class="pd-2">PPh PASAL 21 KURANG BAYAR/LEBIH BAYAR MASA PAJAK TERAKHIR</td>
								<td class="pd-2" style="text-align: right;">
									<?=isset($data->dec_jumlah_19)? number_format($data->dec_jumlah_23, 0, ',', '.' ) : '-'?>
								</td>
							</tr>
							<tr>
								<td class="pd-2" width="20px"></td>
								<td class="pd-2">23a. PPh PASAL 21 DIPOTONG</td>
								<td class="pd-2" style="text-align: right;">
									<?=isset($data->dec_jumlah_19)? number_format($data->dec_jumlah_23a, 0, ',', '.' ) : '-'?>
								</td>
							</tr>
							<tr>
								<td class="pd-2" width="20px"></td>
								<td class="pd-2">23b. PPh PASAL 21 DITANGGUNG PEMERINTAH (DTP)</td>
								<td class="pd-2" style="text-align: right;">
									<?=isset($data->dec_jumlah_19)? number_format($data->dec_jumlah_23b, 0, ',', '.' ) : '-'?>
								</td>
							</tr>

						</table>
					</div>

					<div id="pemotong_bupot" style="padding:5px; margin-top:5px">
						<div>C. IDENTITAS PEMOTONG</div>
						<table width="100%" style="border: 1px solid; font-size: 11px; padding: 5px 0;">
							<tr>
								<td class="no-border" width="10%">1. NPWP</td>
								<td class="no-border" width="45%">:
									<span style="font-size: 8px;color:#428df5;">C.01</span>
									<span style="width: 100px !important; border-bottom: 1px solid; display: inline-block;">
										<?=isset($data->var_npwp_pemotong)? substr($data->var_npwp_pemotong, 0, 2) : ''?>.
										<?=isset($data->var_npwp_pemotong)? substr($data->var_npwp_pemotong, 2, 3) : ''?>.
										<?=isset($data->var_npwp_pemotong)? substr($data->var_npwp_pemotong, 5, 3) : ''?>.
										<?=isset($data->var_npwp_pemotong)? substr($data->var_npwp_pemotong, 8, 1) : ''?>
									</span>-
									<span style="width: 50px !important; border-bottom: 1px solid; display: inline-block;">
										<?=isset($data->var_npwp_pemotong)? substr($data->var_npwp_pemotong, 9, 3) : ''?>
									</span>.
									<span style="width: 50px !important; border-bottom: 1px solid; display: inline-block;">
										<?=isset($data->var_npwp_pemotong)? substr($data->var_npwp_pemotong, 12, 3) : ''?>
									</span>
								</td>
								<td class="no-border" width="25%" style="padding-left:20px">3. TANGGAL & TANDA TANGAN</td>
								<td class="no-border" width="20%" rowspan="2">
									<div style="width: 100%; height: 75px; border: 1px solid; text-align:center; padding:5px 2px">
										<img style="max-width: 90%; max-height:90%; margin:auto" src="<?=base_url()?><?=isset($data->var_ttd_stempel)? $data->var_ttd_stempel : '/assets/images/ttd-stempel-jai.jpg'?>">
									</div>
								</td>
							</tr>
							<tr>
								<td class="no-border" width="10%" style="padding: 15px 5px;">2. NAMA</td>
								<td class="no-border" width="45%" style="padding: 15px 5px;">: 
									<span style="font-size: 8px;color:#428df5;">C.02</span>
									<span style="width: calc( 100% - 30px) !important; border-bottom: 1px solid; display: inline-block;">
										<?=isset($data->var_nama_pemotong)? $data->var_nama_pemotong : ''?>
									</span></td>
								<td class="no-border" width="25%" style="padding: 15px 5px 5px;">
									<span style="font-size: 8px;color:#428df5;">C.03</span>
									<span style="width: calc( 100% - 30px) !important; border-bottom: 1px solid; display: inline-block;">
									<?php
										if(isset($data->dt_bukti_potong)){
											$tanggal = strtotime($data->dt_bukti_potong);
											$format_tanggal = date("d-m-Y", $tanggal);
											echo $format_tanggal;
										}else{
											echo '-';
										}
									?>
									</span><br>
									<span style="font-size: 10px; margin-left: 20px;">[ dd - mm - yyy]</span>
								</td>
							</tr>

						</table>
						<div style="height:12px; width:23px; margin-top: 20px; float: left;">
							<img src="<?=base_url()?>assets/images/black.png" width="100%" height="100%">
						</div>
						<div style="height:12px; width:23px; margin-top: 20px; float: right;">
							<img src="<?=base_url()?>assets/images/black.png" width="100%" height="100%">
						</div>
					</div>

                </div>
            </div>
        </div>
        <div class="d-print-none text-right" style="margin:15px 0;">
            <a href="<?=$url?>" class="btn btn-sm btn-danger tooltips"><i class="fas fa-reply"> </i> Kembali</a>
            <a href="#" onclick="window.print()" class="btn btn-sm btn-primary tooltips"><i class="fas fa-print"> </i> Print</a>
        </div>
        </section>
    </div>
</div>