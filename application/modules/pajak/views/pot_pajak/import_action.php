<form method="post" action="<?=$url ?>" role="form" class="form-horizontal" id="import-form" width="80%">
<div id="modal-import" class="modal-dialog modal-md" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?=$title?></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="form-message text-center"></div>
			<div class="form-group row mb-1">
				<label for="customFile" class="col-sm-4 col-form-label">File Import</label>
				<div class="col-sm-8">
					<div class="custom-file">
						<input type="file" class="custom-file-input" name="<?php echo $input_file_name?>" id="customFile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"/>
						<label class="custom-file-label" for="customFile">Pilih File</label>
					</div>
				</div>
			</div>
			<div class="form-group row mb-3">
				<label for="mulai" class="col-sm-4 col-form-label">Mulai Baris</label>
				<div class="col-sm-2">
					<input type="text" id="mulai" name="mulai" class="form-control form-control-sm currency text-right" value="2">
				</div>
				<i class="col-sm-6 required">*Baris data pada file excel</i>
			</div>
			<div class="form-group row mb-1 mt-3">
				<label for="dt_periode" class="col-sm-4 col-form-label">Periode Pajak</label>
				<div class="col-sm-8">
					<input type="text" id="dt_periode" name="dt_periode" class="form-control form-control-sm date_picker text-right" value="<?=isset($data->dt_periode)? $data->dt_periode : ''?>" />
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="dt_email_dikirim" class="col-sm-4 col-form-label">Tanggal Kirim</label>
				<div class="col-sm-8">
					<input type="text" id="dt_email_dikirim" name="dt_email_dikirim" class="form-control form-control-sm date_picker text-right" value="<?=isset($data->dt_email_dikirim)? $data->dt_email_dikirim : ''?>" />
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="var_subjek_email" class="col-sm-4 col-form-label">Subjek eMail</label>
				<div class="col-sm-8">
					<input type="text" id="var_subjek_email" name="var_subjek_email" class="form-control form-control-sm" value="<?=isset($data->var_subjek_email)? $data->var_subjek_email : ''?>" />
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="txt_isi_email" class="col-sm-4 col-form-label">Isi eMail</label>
				<div class="col-sm-8">
					<textarea class="form-control form-control-sm textarea" name="txt_isi_email" id="summernote" placeholder="isi email ..."><?=isset($data->txt_isi_email)? $data->txt_isi_email : ''?></textarea>
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="txt_isi_email" class="col-sm-4 col-form-label">Tanda Tangan & Stempel</label>
				<div class="col-sm-8">
					<div class="input-group custom-file">
						<input type="file" class="custom-file-input input_foto" name="var_ttd_stempel" id="var_ttd_stempel" accept="image/*">
						<div class="input-group-prepend">
							<span class="custom-file-label" for="input_foto">Pilih Gambar atau Foto</span>
						</div>
					</div>
					<div id="view_foto_ttd_stempel" class="img-upload-area">
						<?php if(isset($data->var_ttd_stempel)): ?>
						<div class="preview-image preview-show img-popup">
							<div class="image-zone" data-responsive="<?=cdn_url().$data->var_ttd_stempel ?> 375" data-src="<?=cdn_url().$data->var_ttd_stempel ?>">
								<a href="<?=cdn_url().$data->var_ttd_stempel ?>">
									<img id="pro-img-view_foto_ttd_stempel" style="height:auto;max-height:100px;width:auto;max-width:100%" src="<?=cdn_url().$data->var_ttd_stempel ?>">
								</a>
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-danger">Batal</button>
			<button type="submit" class="btn btn-success">Simpan</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	$(document).ready(function(){
		$('.date_picker').daterangepicker(datepickModal);
		$(".img-popup").lightGallery();
		$('.currency').inputmask("numeric", {
            autoUnmask: true,
            radixPoint: ",",
            groupSeparator: ".",
            digits: 2,
            autoGroup: true,
            prefix: '',
            rightAlign: true,
            positionCaretOnClick: true,
        });
		$('#summernote').summernote({
			dialogsInBody: true,
			height: 200,                 // set editor height
			minHeight: null,             // set minimum height of editor
			maxHeight: null,             // set maximum height of editor
			focus: true                  // set focus to editable area after initializing summernote
		});
		$("#import-form").validate({
			rules: {
				<?php echo $input_file_name?>: {
					required: true,
					accept: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel",
				},
				mulai:{
			        required: true,
					digits:true
				},
				dt_email_dikirim:{
			        required: true
				},
				var_subjek_email:{
			        required: true,
					minlength: 5
				},
				txt_isi_email:{
			        required: true,
					minlength: 10
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
				blockUI('#modal-import', 'progress', 4);
                //let blc = '#modal-import';
                //blockUI(blc);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
						setFormMessage('.form-message', data);
						if(data.stat){
							dataTable.draw();
							resetForm(form)
						}
						if(data.hasOwnProperty('files')){
							$.each(data.files, function(i, v){
								if(v.stat){
									if(!$('.'+v.cdImg).hasClass('is-valid')) $('.title-image-'+i).addClass('is-valid');
									if(!$('.'+v.cdImg).next('div').hasClass('valid-feedback'))$('.title-image-'+i).after('<div id="'+i+'-valid" class="valid-feedback">'+v.msg+'</div>');
								}else{
									if(!$('.'+v.cdImg).hasClass('is-invalid')) $('.'+v.cdImg).addClass('is-invalid');
									if(!$('.'+v.cdImg).next('div').hasClass('invalid-feedback'))$('.'+v.cdImg).after('<div id="'+i+'-error" class="invalid-feedback">'+v.msg+'</div>');
								}
								
								
							});
						}else{
							blockUI('#modal-import', 'progress', 1);
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});

		bsCustomFileInput.init();
		$( ".preview-images-zone" ).sortable();
		$(document).on('click', '.image-cancel', function() {
			let no = $(this).data('no');
			//$(".preview-image.preview-show-"+no).parent().parent().remove();
			$(this).parent().parent().remove();
		});

		$('#var_ttd_stempel').change(function(){
			preview_img_upload('ttd_stempel', 'lg');
		});

		function preview_img_upload(preview_id, preview_size){
			if (window.File && window.FileList && window.FileReader) {
				var files = event.target.files; //FileList object
				img_preview_size = 'style="height:auto;max-height:100px;width:auto;max-width:100%"';
				for (let i = 0; i < 1; i++) {
					var file = files[0];
					if (!file.type.match('image')) continue;
					
					var picReader = new FileReader();
					let cdImg 	= 'img'+getSandStr(3,'-',3);
					
					picReader.addEventListener('load', function (event) {
						var picFile = event.target;
						var html =  '<div class="img-client"><input type="hidden" name="cdImg['+preview_id+']" value="'+cdImg+'"/>' +
									'<div class="preview-image preview-show-'+preview_id+'">' +
									'<div class="image-zone"><img  '+img_preview_size+' id="pro-img-'+preview_id+'" src="'+picFile.result+'"></div>' +
									'</div><input type="hidden" class="form-control form-control-sm '+cdImg+'" disabled/></div>';

						document.getElementById('view_foto_'+preview_id+'').innerHTML = html;
					});

					picReader.readAsDataURL(file);
				}
			}else {
				alert('Browser not support');
			}
		}
	});
</script>