<link rel="stylesheet" href="<?=base_url() ?>assets/plugins/summernote/summernote-bs4.css">
<link rel="stylesheet" href="<?=base_url() ?>assets/plugins/lightGallery/dist/css/lightgallery.css">
<style>
.img-thumb-sm {
    max-width: 200px;
    max-height: 110px;
}

.img-thumb{
    width:100%;
    height:120px;
    border:0 solid rgba(0,0,0,.125);
    border-radius:3px;
    padding:5px;
    box-shadow: 0 0 1px rgba(0,0,0,.125),0 1px 3px rgba(0,0,0,.2);
}

.img-upload-area{
    text-align: center;
    background: #e0e4e7;
    padding: 6px 3px 3px 3px;
    margin-top: -3px;
    margin-bottom: 17px;
    border-radius: 3px;
    height:110px;
}

.img-upload-area-sm{
    text-align: center;
    background: #e0e4e7;
    padding: 6px 3px 3px 3px;
    margin-top: -3px;
    margin-bottom: 37px;
    border-radius: 3px;
    height:110px;
}
</style>