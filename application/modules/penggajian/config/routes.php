<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['gaji']['get']                       = 'penggajian/gaji';
$route['gaji']['post']                      = 'penggajian/gaji/list';
$route['gaji/add']['get']                   = 'penggajian/gaji/add';
$route['gaji/save']['post']                 = 'penggajian/gaji/save';
$route['gaji/([a-zA-Z0-9]+)']['get']        = 'penggajian/gaji/get/$1';
$route['gaji/([a-zA-Z0-9]+)']['post']       = 'penggajian/gaji/update/$1';
$route['gaji/([a-zA-Z0-9]+)/del']['get']    = 'penggajian/gaji/confirm/$1';
$route['gaji/([a-zA-Z0-9]+)/del']['post']   = 'penggajian/gaji/delete/$1';

$route['detail/([a-zA-Z0-9]+)']['get']      = 'penggajian/detail/index/$1';
$route['detail/([a-zA-Z0-9]+)']['post']     = 'penggajian/detail/list/$1';
$route['detail/([a-zA-Z0-9]+)/slip']['get'] = 'penggajian/detail/get_gaji/$1';
$route['detail/([a-zA-Z0-9]+)/resend']['get']   = 'penggajian/detail/confirm/$1';
$route['detail/([a-zA-Z0-9]+)/resend']['post']   = 'penggajian/detail/resend/$1';
$route['detail/resendall/([a-zA-Z0-9]+)']['get']   = 'penggajian/detail/confirm_resendall/$1';
$route['detail/resendall/([a-zA-Z0-9]+)']['post']   = 'penggajian/detail/resendall/$1';
$route['export/detail/([a-zA-Z0-9]+)']['post']      = 'penggajian/detail/export/$1';

$route['nik/cek']['post']           = 'master/nik/cek';

$route['wilayah/kelurahan']['post'] = 'master/wilayah/get_kelurahan';
$route['wilayah/rw']['post']        = 'master/wilayah/get_rw';
$route['wilayah/rt']['post']        = 'master/wilayah/get_rt';
