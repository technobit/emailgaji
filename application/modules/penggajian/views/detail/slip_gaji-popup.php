<?//=print_r($penerimaan)?>
<form method="post" action="<?=$url ?>" role="form" class="form-horizontal" id="import-form" width="90%">
<div id="modal-import" class="modal-dialog modal-xl" role="document">
	<div class="modal-content">
		<div class="modal-body" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px">
			<div id="container" class="d-print-block" style="max-width:210mm;margin:auto;border:3px solid;">
				<div id="left" style="width:34%;float:left">
					<div id="left-content" style="padding:10px">
						<table style="width:100%;">
							<tr>
								<th colspan="3" style="padding:5px; border-bottom: 2px solid;text-align: center;">STRUK GAJI</th>
							</tr>
							<tr>
								<td align="left">BULAN</td>
								<td align="left"><?=idn_date($d->dt_periode, "F Y")?></td>
							</tr>
							<tr>
								<td align="left">NIK</td>
								<td align="left"><?=$data->a?></td>
							</tr>
							<tr>
								<td align="left">NAMA</td>
								<td align="left"><?=$data->b?></td>
							</tr>
							<tr>
								<td colspan="2" style="height: 5mm;"></td>
							</tr>
							<tr>
								<td align="left">LOKASI</td>
								<td align="left"><?=$data->c?></td>
							</tr>
							<tr>
								<td align="left">BAGIAN</td>
								<td align="left"><?=$data->d?></td>
							</tr>
							<tr>
								<td align="left">VALUTA</td>
								<td align="left"><?=$data->e?></td>
							</tr>
							<tr>
								<td colspan="2" style="height: 10mm;"><span style="display:block;border-top:1px solid #000"></span></td>
							</tr>
							<tr>
								<td align="left">Sisa Plafon</td>
								<td align="left"><?=isset($data->bs)? $data->bs : '-'?></td>
							</tr>
							<tr>
								<td align="left">Sisa Cuti</td>
								<td align="left"><?=isset($data->bt)? $data->bt : '-'?> Hari</td>
							</tr>
							<tr>
								<td align="left">Pesan</td>
								<td align="left"></td>
							</tr>
							<tr>
								<td align="left"colspan="2"><?=isset($data->bu)? $data->bu : '-'?></td>
							</tr>
						</table>
					</div>
				</div>
				<div id="right" style="width:66%;float:left">
					<div id="right-content" style="border-left: 1px solid;margin-left:-1px">
						<div id="penerimaan" style="width:50%;float:left;">
							<table style="width:100%;padding:10px;border-right: 1px solid;border-collapse: unset;">
								<tr>
									<th colspan="2" style="padding:5px; border-bottom: 2px solid;text-align: center;">PENERIMAAN (Gaji & Tunjangan)</th>
								</tr>
								<?php foreach($penerimaan as $col => $val):
									if($val != 0){?>
								<tr>
									<td align="left"><?=str_replace('"','',$col)?></td>
									<td align="right"><?=number_format($val,0, ',', '.' )?></td>
								</tr>
								<?php } endforeach; ?>
							</table>
						</div>
						<div id="potongan" style="width:50%;float:left">
							<table style="width:100%;;padding:10px;border-collapse: unset;border-left: 1px solid;margin-left: -1px;">
								<tr>
									<th colspan="4" style="padding:5px; border-bottom: 2px solid;text-align: center;">POTONGAN</th>
								</tr>
								<tr>
									<td align="left" colspan="2">Pajak</td>
									<td align="right"><?=number_format($data->aq,0, ',', '.' )?></td>
								</tr>
								<tr>
									<td align="left" colspan="2">Potongan</td>
									<td align="right"><?=number_format($data->bm,0, ',', '.' )?></td>
								</tr>
								<?php foreach($potongan as $col => $val):
									if($val != 0){?>
								<tr>
									<td align="left">- <?=str_replace('"','',$col)?></td>
									<td align="right"><?=number_format($val,0, ',', '.' )?></td>
									<td align="right"></td>
								</tr>
								<?php } endforeach; ?>
							</table>
						</div>
						<div id="total" style="clear: both;border-top: 1px solid">
							<div id="total-penerimaan" style="width:50%;float:left;">
								<table style="width:100%;padding:10px;border-right: 1px solid;border-collapse: unset;">
									<tr>
										<td align="left">Pendapatan Bruto (A)</td>
										<td align="right"><?=number_format($data->ap,0, ',', '.' )?></td>
									</tr>
								</table>
							</div>
							<div id="total-potongan" style="width:50%;float:left">
								<table style="width:100%;padding:10px;border-collapse: unset;">
									<tr>
										<td align="left">Total Potongan (B)</td>
										<td align="right"><?=number_format(($data->bm + $data->aq),0, ',', '.' )?></td>
									</tr>
								</table>
							</div>
						</div>
						<div id="rekapitulasi" style="clear:both;border-top:2px solid">
							<table style="width:100%;padding:10px;border-collapse: unset;">
								<tr>
									<td align="left">PENDAPATAN BERSIH (A - B)</td>
									<td align="right">: Rp</td>
									<td align="right"><?=number_format($data->bn,0, ',', '.' )?></td>
									<td align="right" width="10"></td>
								</tr>
								<tr>
									<td align="left">PDP</td>
									<td align="right">: Rp</td>
									<td align="right"><?=number_format($data->bo,0, ',', '.' )?></td>
									<td align="right"></td>
								</tr>
								<tr>
									<td colspan="4" align="right">__________________ +</td>
								</tr>
								<tr style="font-weight: bold;">
									<td align="left">DIBAYARKAN</td>
									<td align="right">: Rp</td>
									<td align="right"><?=number_format($data->bp,0, ',', '.' )?></td>
									<td align="right"></td>
								</tr>
								<tr>
									<td colspan="4" style="height: 5mm;"><span style="display:block"></span></td>
								</tr>
								<?php if ($data->bq == 'B'){
										$info = 'ditransfer ke rekening '.$data->br;
									}else{
										$info = 'dilakukan secara tunai';
									}
								?>
								<tr>
									<td colspan="4" style="height: 10mm;text-align: left;border-top:1px solid">Pembayaran gaji sebesar Rp <?=number_format($data->bp,0, ',', '.' )?> <?=$info?></span></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<div style="clear: both;margin-left:auto;margin-right: auto">
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-warning">Keluar</button>
			<button type="submit" class="btn btn-success">Save</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	$(document).ready(function(){
		$('.date_picker').daterangepicker(datepickModal);
		bsCustomFileInput.init();
		$('.currency').inputmask("numeric", {
            autoUnmask: true,
            radixPoint: ",",
            groupSeparator: ".",
            digits: 2,
            autoGroup: true,
            prefix: '',
            rightAlign: true,
            positionCaretOnClick: true,
        });
		$("#import-form").validate({
			rules: {
				dt_gaji_dikirim:{
			        required: true
				},
				var_subjek_email:{
			        required: true,
					minlength: 5
				},
				txt_isi_email:{
			        required: true,
					minlength: 10
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
                let blc = '#modal-import';
                blockUI(blc);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
						setFormMessage('.form-message', data);
						if(data.stat){
							dataTable.draw();
							resetForm(form)
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});
	});
</script>