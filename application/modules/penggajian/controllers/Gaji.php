<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Gaji extends MX_Controller {
	private $input_file_name = 'data_gaji';
	private $import_dir = 'assets/import';

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'GAJI';
		$this->module   = 'penggajian';
		$this->routeURL = 'gaji';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('Gaji_model', 'model');

    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Data Penggajian';
		$this->page->menu 	  = 'gaji';
		//$this->page->submenu1 = 'penggajian';
		$this->breadcrumb->title = 'Data Pengajian';
		$this->breadcrumb->card_title = 'Riwayat Data Penggajian';
		$this->breadcrumb->icon = 'fas fa-money-bill-wave-alt';
		$this->breadcrumb->list = ['Penggajian'];
		$this->js = true;
		$this->css = true;
		$data['url'] = site_url("{$this->routeURL}/add");
		$this->render_view('gaji/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$totald = $this->model->listCount($this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			$data[] = array($i.'. ', idn_date($d->import_date, "j F Y H:i:s"), $d->file_name, $d->total, idn_date($d->dt_email_dikirim), $d->created, $d->updated, $d->h_id);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $totald,
								'iTotalDisplayRecords' => $totald,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()));
	}

	public function add(){
		if($this->authCheckDetailAccess('c', true) == false) return; // hak akses untuk modal popup
		$email = $this->model->get_last_email(0);

		$data['data']		= $email;
		$data['url']        = site_url("{$this->routeURL}/save");
		$data['title']      = 'Import Data Gaji';
		$data['input_file_name'] = $this->input_file_name;
		$this->load_view('gaji/import_action', $data, true);
		
	}

	public function save(){
		$this->authCheckDetailAccess('c');
		
		$start = start_time();
		$this->form_validation->set_rules('mulai', 'Mulai', 'required|integer');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'mc' => false, //modal close
								'msg' => "Terjadi kesalahan",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        }else{
			if(isset($_FILES[$this->input_file_name])){
				$config['upload_path']   = "./{$this->import_dir}"; 
				$config['allowed_types'] = 'xlsx|xls'; 
				$config['encrypt_name']  = true; 
				$config['max_size']      = 4096;  
				$this->load->library('upload', $config);
				
				if($this->upload->do_upload($this->input_file_name)){
					$status  = $this->model->import($this->input->post(), $this->upload->data());
					$this->set_json([  'stat' => ($status !== false), 
								'mc' => ($status !== false), //modal close
								'time' => finish_time($start),
								'msg' => ($status !== false)? "Data berhasil di-import dengan {$status} baris data." : 'Data gagal di-import',
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);
				} else {
					$this->set_json([  'stat' => false, 
										'mc' => false, //modal close
										'msg' => "Data gagal di-import. ".$this->upload->display_errors('',''),
										'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
									]);
				}
			} else {
				$this->set_json([  'stat' => false, 
									'mc' => false, //modal close
									'msg' => "Data gagal di-import",
									'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
								]);
			}

        }
	}

	public function get($h_id){
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($h_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Error', 'title' => 'posts Not Found', 'message' => '']],true);
		}else{
			$data['data'] 	= $res;
			$data['url']	= site_url("{$this->routeURL}/$h_id");
			$data['title']	= 'Edit Data';
			$this->load_view('gaji/edit_action', $data);
		}
		
	}

	public function update($h_id){
		$this->authCheckDetailAccess('u');
		
		$this->form_validation->set_rules('txt_category', 'Category', 'alpha_numeric_spaces|min_length[4]|max_length[100]');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
            $check = $this->model->update($h_id, $this->input_post());
			$this->set_json([  'stat' => $check, 
								'mc'   => $check, //modal close
								'msg'  => ($check)? "Data Updated Successfully" : "Data Update Failed",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);

        }
	}

	public function confirm($h_id){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($h_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['url']	= site_url("{$this->routeURL}/$h_id/del");
			$data['title']	= 'Hapus Data Penggajian';
			$data['info']   = [ 'Tanggal Upload' => idn_date($res->import_date, "j F Y H:i:s"),
                                'Total Data' => $res->total,
                                'Jadwal Kirim' => idn_date($res->dt_email_dikirim)];
			$this->load_view('gaji/delete_confirm', $data);
		}
	}

	public function delete($h_id){
		$this->authCheckDetailAccess('d');

		$check = $this->model->delete($h_id);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Data Deleted Successfully" : "Data Delete Failed",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
		
	}

	public function detail($id_import){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Data Penggajian';
		$this->page->menu 	  = 'gaji';
		//$this->page->submenu1 = 'penggajian';
		$this->breadcrumb->title = 'Data Pengajian';
		$this->breadcrumb->card_title = 'Riwayat Data Penggajian';
		$this->breadcrumb->icon = 'fas fa-money-bill-wave-alt';
		$this->breadcrumb->list = ['Penggajian'];
		$this->js = true;
		$data['url'] = site_url("{$this->routeURL}/add");
		$this->render_view('gaji/index', $data, true);
	}

	public function detail_list($id_import){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$totald = $this->model->listCount($this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			$data[] = array($i.'. ', idn_date($d->import_date, "j F Y H:i:s"), $d->file_name, $d->total, idn_date($d->dt_email_dikirim), $d->h_id);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $totald,
								'iTotalDisplayRecords' => $totald,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()));
	}

}
