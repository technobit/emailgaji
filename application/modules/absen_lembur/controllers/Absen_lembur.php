<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absen_lembur extends MX_Controller {
	private $input_file_name = 'data_absen_lembur';
	private $import_dir = 'assets/import';

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'ABSEN-LEMBUR';
		$this->module   = 'absen_lembur';
		$this->routeURL = 'absen_lembur';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('Absen_lembur_model', 'model');

    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Data Absensi & Lembur';
		$this->page->menu 	  = 'absen_lembur';
		$this->breadcrumb->title = 'Data Absensi & Lembur';
		$this->breadcrumb->card_title = 'Riwayat Data Absensi & Lembur';
		$this->breadcrumb->icon = 'fas fa-user-check';
		$this->breadcrumb->list = ['Absensi & Lembur'];
		$this->js = true;
		$this->css = true;
		$data['url'] = site_url("{$this->routeURL}/add");
		$this->render_view('absen_lembur/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$totald = $this->model->listCount($this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			$data[] = array($i.'. ', idn_date($d->import_date, "j F Y H:i:s"), idn_date($d->dt_periode_awal, "j F Y").' s.d. <br>'.idn_date($d->dt_periode_akhir, "j F Y"),
			$d->file_name.'<br>'.$d->file_name_2, $d->total.'<br>'.$d->total_2, idn_date($d->dt_email_dikirim), $d->created, $d->h_id);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $totald,
								'iTotalDisplayRecords' => $totald,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()));
	}

	public function add(){
		if($this->authCheckDetailAccess('c', true) == false) return; // hak akses untuk modal popup
		$email = $this->model->get_last_email(2);

		$data['data']		= $email;
		$data['url']        = site_url("{$this->routeURL}/save");
		$data['title']      = 'Import Data Absensi & Lembur';
		//$data['input_file_name'] = $this->input_file_name;
		$this->load_view('absen_lembur/import_action', $data, true);
		
	}

	public function save(){
		$this->authCheckDetailAccess('c');
		
		$start = start_time();
		$this->form_validation->set_rules('mulai_baris_absensi', 'Mulai Baris Absensi', 'required|integer');
		$this->form_validation->set_rules('mulai_baris_lembur', 'Mulai Baris Lembur', 'required|integer');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'mc' => false, //modal close
								'msg' => "Terjadi kesalahan",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        }else{
			if(isset($_FILES['excel_absensi'])){
				$import_absensi = $this->import_excel($this->input->post(), $_FILES['excel_absensi'], 'excel_absensi', 1);
				//berhasil import absensi
				if($import_absensi['status']){
					$import_lembur = $this->import_excel($this->input->post(), $_FILES['excel_lembur'], 'excel_lembur', 2, $import_absensi['id_import']);
					//berhasil import lembur
					if($import_lembur['status']){
						$this->set_json([  'stat' => true, 
											'mc' => true, //modal close
											'time' => finish_time($start),
											'msg' => true? "Data berhasil di-import" : 'Data gagal di-import',
											'csrf' => [ 'name' => $this->getCsrfName(),
														'token' => $this->getCsrfToken()]
										]);
					//gagal import lembur
					}else{
						//########## hapus data import absensi
						$cancel = $this->model->delete($import_absensi['id_import']);
						//########## hapus data import absensi
						//########## hapus data import absensi
						//########## hapus data import absensi

						$this->set_json([  'stat' => false, 
											'mc' => false, //modal close
											'msg' => "Data gagal di-import. ".$this->upload->display_errors('',''),
											'csrf' => [ 'name' => $this->getCsrfName(),
												'token' => $this->getCsrfToken()]
										]);
					}

				//gagal import absensi
				}else{
					$this->set_json([  'stat' => false, 
										'mc' => false, //modal close
										'msg' => "Data gagal di-import. ".$this->upload->display_errors('',''),
										'csrf' => [ 'name' => $this->getCsrfName(),
											'token' => $this->getCsrfToken()]
									]);
				}
			}
		}
	}

	function import_excel($data, $files, $file_name, $type, $id_import = ''){ //type = 1 absensi; 2 lembur | id_import -> lembur
		$config['upload_path']   = "./{$this->import_dir}"; 
		$config['allowed_types'] = 'xlsx|xls'; 
		$config['encrypt_name']  = true; 
		$config['max_size']      = 4096;  
		$this->load->library('upload', $config);

		if($this->upload->do_upload($file_name)){
			if($type == 1){
				$import = $this->model->import_absensi($data, $this->upload->data());
				$ret['status'] = true;
				$ret['total_row'] = $import['total_row'];
				$ret['id_import'] = $import['id_import'];
			}else if($type == 2){
				$import = $this->model->import_lembur($data, $this->upload->data(), $id_import);
				$ret['status'] = true;
				$ret['total_row'] = $import['total_row'];
				$ret['id_import'] = null;
			}else{
				$ret['status'] = false;
				$ret['total_row'] = null;
				$ret['id_import'] = null;	
			}
		}else{
			$ret['status'] = false;
			$ret['total_row'] = null;
			$ret['id_import'] = null;
		}
		$this->upload->error_msg = [];

		return $ret;
	}

	public function get($h_id){
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($h_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Error', 'title' => 'posts Not Found', 'message' => '']],true);
		}else{
			$data['data'] 	= $res;
			$data['url']	= site_url("{$this->routeURL}/$h_id");
			$data['title']	= 'Edit Data';
			$this->load_view('absen_lembur/edit_action', $data);
		}
		
	}

	public function update($h_id){
		$this->authCheckDetailAccess('u');
		
		$this->form_validation->set_rules('var_subjek_email', 'Subjek Email', 'required');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
            $check = $this->model->update($h_id, $this->input_post());
			$this->set_json([  'stat' => $check, 
								'mc'   => $check, //modal close
								'msg'  => ($check)? "Data Updated Successfully" : "Data Update Failed",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);

        }
	}

	public function confirm($h_id){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($h_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['url']	= site_url("{$this->routeURL}/$h_id/del");
			$data['title']	= 'Hapus Data Penggajian';
			$data['info']   = [ 'Tanggal Upload' => idn_date($res->import_date, "j F Y H:i:s"),
                                'Subjek Email' => $res->var_subjek_email,
                                'Jadwal Kirim' => idn_date($res->dt_email_dikirim)];
			$this->load_view('absen_lembur/delete_confirm', $data);
		}
	}

	public function delete($h_id){
		$this->authCheckDetailAccess('d');

		$check = $this->model->delete($h_id);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Data Deleted Successfully" : "Data Delete Failed",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
		
	}

	public function detail($id_import){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Data Absensi & Lembur';
		$this->page->menu 	  = 'absen_lembur';
		//$this->page->submenu1 = 'penggajian';
		$this->breadcrumb->title = 'Data Absensi & Lembur';
		$this->breadcrumb->card_title = 'Riwayat Data Absensi & Lembur';
		$this->breadcrumb->icon = 'fas fa-money-bill-wave-alt';
		$this->breadcrumb->list = ['Absensi & Lembur', 'Detil'];
		$this->js = true;
		$data['url'] = site_url("{$this->routeURL}/add");
		$this->render_view('absen_lembur/index', $data, true);
	}

	public function detail_list($id_import){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$totald = $this->model->listCount($this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			$data[] = array($i.'. ', idn_date($d->import_date, "j F Y H:i:s"), $d->file_name, $d->total, idn_date($d->dt_email_dikirim), $d->h_id);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $totald,
								'iTotalDisplayRecords' => $totald,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()));
	}
}
