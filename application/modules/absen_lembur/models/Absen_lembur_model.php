<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;

class Absen_lembur_model extends MY_Model {


	public function get_last_email($int_type){
		return $this->db->query("SELECT txt_isi_email, var_subjek_email
								 FROM	{$this->h_import}
								 WHERE int_type = {$int_type}
								 ORDER BY h_id DESC LIMIT 1")->row();
	}

    public function list($filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$this->db->select("*, CONCAT(suc.txt_nama_depan, ' ', suc.txt_nama_belakang) as created,
		CONCAT(suu.txt_nama_depan, ' ', suu.txt_nama_belakang) as updated")
					->from($this->h_import." hi")
					->join($this->s_user." suc", "hi.created_by = suc.int_id_user", "left")
					->join($this->s_user." suu", "hi.updated_by = suu.int_id_user", "left")
					->where('hi.int_type', 2);


		if(($this->session->userdata['int_level'] == 2)){
			$this->db->where('hi.int_level', 2);
		}
			
		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('import_date', $filter)
					->or_like('file_name', $filter)
					->or_like('total', $filter)
					->or_like('dt_email_dikirim', $filter)
					->group_end();
		}

		switch($order_by){
			case 1 : $order = 'import_date '; break;
			case 2 : $order = 'file_name '; break;
			case 3 : $order = 'total '; break;
			case 4 : $order = 'dt_email_dikirim '; break;
			default: {$order = 'import_date '; $sort = 'DESC';} break;

		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($filter = NULL){
		$this->db->from($this->h_import)
		->where('int_type', 2);

        if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
            $this->db->group_start()
			->like('import_date', $filter)
			->or_like('file_name', $filter)
			->or_like('total', $filter)
			->or_like('dt_email_dikirim', $filter)
			->group_end();
        }
		return $this->db->count_all_results();
	}

	public function get($h_id){
		return $this->db->select("hi.*, tal.dt_periode_awal, tal.dt_periode_akhir")
					->join($this->t_absensi_lembur.' tal', 'hi.`h_id` = tal.`id_import`', 'left')
					->get_where($this->h_import.' hi', ['h_id' => $h_id])->row();
	}

	public function update($h_id, $upd){
		$upd_import['var_subjek_email'] = $upd['var_subjek_email'];
		$upd_import['txt_isi_email'] = $upd['txt_isi_email'];
		$upd_import['dt_email_dikirim'] = $upd['dt_email_dikirim'];
		$upd_import['updated_by'] = $this->session->userdata['user_id'];
		$upd_import['dt_periode_awal'] = $upd['dt_periode_awal'];
		$upd_import['dt_batas_revisi'] = $upd['dt_batas_revisi'];
		$upd_import['time_batas_jam_revisi'] = $upd['time_batas_jam_revisi'];

		$upd_absensi_lembur['dt_periode_awal'] = $upd['dt_periode_awal'];
		$upd_absensi_lembur['dt_periode_akhir'] = $upd['dt_periode_akhir'];
		$upd_absensi_lembur['dt_email_dikirim'] = $upd['dt_email_dikirim'];

		$this->db->trans_begin();

		$this->db->where('h_id', $h_id);
		$this->db->update($this->h_import, $upd_import);

		$upd_data_absensi = $this->upd_absensi_lembur($h_id, $upd_absensi_lembur, 'h_absensi');
		$upd_data_lembur = $this->upd_absensi_lembur($h_id, $upd_absensi_lembur, 'h_lembur');
		$upd_rekap = $this->upd_absensi_lembur($h_id, $upd_absensi_lembur, 't_absensi_lembur');

		if ($this->db->trans_status() === FALSE || $upd_data_absensi === FALSE || $upd_data_lembur === FALSE || $upd_rekap === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function upd_absensi_lembur($id_import, $upd, $table){
		$this->db->trans_begin();

		$this->db->where('id_import', $id_import);
		$this->db->update($table, $upd);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete($h_id){
		$this->db->trans_begin();
		$this->db->delete($this->h_import,  ['h_id' => $h_id]);

		$del_data_absensi = $this->del_absensi_lembur($h_id, 'h_absensi');
		$del_data_lembur = $this->del_absensi_lembur($h_id, 'h_lembur');
		$del_rekap = $this->del_absensi_lembur($h_id, 't_absensi_lembur');

		if ($this->db->trans_status() === FALSE || $del_data_absensi === FALSE || $del_data_lembur === FALSE || $del_rekap === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function del_absensi_lembur($id_import, $table){
		$this->db->trans_begin();
		$this->db->delete($table,  ['id_import' => $id_import]);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function import_absensi($ins, $file_absensi){
		$user 	= $this->session->userdata('username');
		$dt_email_dikirim = $ins['dt_email_dikirim'];
		$dt_periode_awal = $ins['dt_periode_awal'];
		$dt_periode_akhir = $ins['dt_periode_akhir'];
		$created_by = $this->session->userdata['user_id'];
		$int_level = $this->session->userdata['int_level'];

		$b = 'B'; $c = 'C'; $d = 'D'; $e = 'E'; $f = 'F'; $g = 'G'; 

		$filterSubset = new MyReadFilter($ins['mulai_baris_absensi'],
						[$b, $c, $d, $e, $f, $g]
					);

		$reader = IOFactory::createReader(ucfirst(ltrim($file_absensi['file_ext'],'.')));
		$reader->setReadDataOnly(true);
		$reader->setReadFilter($filterSubset);
		$spreadsheet = $reader->load($file_absensi['full_path']);
		$data = $spreadsheet->getActiveSheet()->toArray(null, true, false, true);
		
		$this->db->trans_begin();
		
		$this->db->insert($this->h_import, ['username' => $user,
										  'file_name' => $file_absensi['orig_name'],
										  'direktori' => $file_absensi['full_path'],
										  'var_subjek_email' => $ins['var_subjek_email'],
										  'txt_isi_email' => $ins['txt_isi_email'],
										  'dt_periode_awal' => $ins['dt_periode_awal'],
										  'dt_periode_akhir' => $ins['dt_periode_akhir'],
										  'dt_email_dikirim' => $ins['dt_email_dikirim'],
										  'dt_batas_revisi' => $ins['dt_batas_revisi'],
										  'time_batas_jam_revisi' => $ins['time_batas_jam_revisi'],
										  'created_by' => $created_by,
										  'int_level' => $int_level,
										  'int_type' => 2
										  ]);
										  
		$id_import = $this->db->insert_id();
		$ins_data_absensi = "INSERT INTO {$this->h_absensi} 
						(`dt_email_dikirim`, `var_nik`, `var_nama`, `var_email`, `var_bagian`, `dt_absensi`,
						`var_keterangan`, `dt_periode_awal`, `dt_periode_akhir`, `id_import`) VALUES ";

		$total = 0;
		foreach($data as $id => $dt){
			if($id > ($ins['mulai_baris_absensi'] - 1)){
				if($dt[$b] && $dt[$d] && $dt[$f] && $dt[$g]){
				$total++;
				$dt_b = $this->db->escape(trim($dt[$b]));
				$dt_c =	$this->db->escape(trim($dt[$c]));
				$dt_d = $this->db->escape(trim($dt[$d]));
				$dt_e = $this->db->escape(trim($dt[$e]));
				//$dt_f = $this->db->escape($dt[$f]);
				$dt_g = $this->db->escape(trim($dt[$g])); 

				$format_tanggal = $this->format_tanggal_excel($dt[$f]);
				$tanggal_absensi = $this->db->escape(trim($format_tanggal));

				$ins_data_absensi .= "('{$dt_email_dikirim}', {$dt_b}, {$dt_c}, {$dt_d}, {$dt_e}, {$tanggal_absensi}, {$dt_g}, 
									'{$dt_periode_awal}', '{$dt_periode_akhir}', {$id_import}),";
				}
			}
		}
		$update_import = $this->update_import($id_import, 'total', $total);

		if($total != 0){
			$ins_data_absensi = rtrim($ins_data_absensi, ',').';';
			$this->db->query($ins_data_absensi);
			if ($this->db->trans_status() === FALSE || $update_import === FALSE){
				$this->db->trans_rollback();
				$return['status'] = false;
				$return['total_row'] = null;
				$return['id_import'] = null;
			}else{
				$this->db->trans_commit();
				$return['status'] = true;
				$return['total_row'] = $total;
				$return['id_import'] = $id_import;
			}
		}else{
			$this->db->trans_commit();
			$return['status'] = true;
			$return['total_row'] = $total;
			$return['id_import'] = $id_import;
		}

		return $return;
	}

	public function import_lembur($ins, $file_lembur, $id_import){
		$user 	= $this->session->userdata('username');
		$dt_email_dikirim = $ins['dt_email_dikirim'];
		$dt_periode_awal = $ins['dt_periode_awal'];
		$dt_periode_akhir = $ins['dt_periode_akhir'];
		$created_by = $this->session->userdata['user_id'];
		$int_level = $this->session->userdata['int_level'];

		$b = 'B'; $c = 'C'; $d = 'D'; $e = 'E'; $f = 'F'; $g = 'G'; $h = 'H'; 

		$filterSubset = new MyReadFilter($ins['mulai_baris_lembur'],
						[$b, $c, $d, $e, $f, $g, $h]
					);

		$reader = IOFactory::createReader(ucfirst(ltrim($file_lembur['file_ext'],'.')));
		$reader->setReadDataOnly(true);
		$reader->setReadFilter($filterSubset);
		$spreadsheet = $reader->load($file_lembur['full_path']);
		$data = $spreadsheet->getActiveSheet()->toArray(null, true, false, true);
		
		$this->db->trans_begin();

		$this->db->where('h_id', $id_import);
		$this->db->update($this->h_import, ['username' => $user,
										  'file_name_2' => $file_lembur['orig_name']
										  ]);
										  
		$ins_data_lembur = "INSERT INTO {$this->h_lembur} 
						(`dt_email_dikirim`, `var_nik`, `var_nama`, `var_email`, `var_bagian`, `dt_lembur`,
						`dec_jam_lembur`, `int_konversi`, `dt_periode_awal`, `dt_periode_akhir`, `id_import`) VALUES ";

		$total = 0;
		foreach($data as $id => $dt){
			if($id > ($ins['mulai_baris_lembur'] - 1)){
				if($dt[$b] && $dt[$d] && $dt[$f] && $dt[$g] && $dt[$h]){
					$total++;
					$dt_b = $this->db->escape(trim($dt[$b]));
					$dt_c =	$this->db->escape(trim($dt[$c]));
					$dt_d = $this->db->escape(trim($dt[$d]));
					$dt_e = $this->db->escape(trim($dt[$e]));
					//$dt_f = $this->db->escape($dt[$f]);
					$dt_g = $this->db->escape($dt[$g]); 
					$dt_h = $this->db->escape($dt[$h]);

					$format_tanggal = $this->format_tanggal_excel($dt[$f]);
					$tanggal_lembur = $this->db->escape(trim($format_tanggal));

					if($dt_b != '' || $dt_c != ''){
						$ins_data_lembur .= "('{$dt_email_dikirim}', {$dt_b}, {$dt_c}, {$dt_d}, {$dt_e}, {$tanggal_lembur}, {$dt_g}, {$dt_h}, 
											'{$dt_periode_awal}', '{$dt_periode_akhir}', {$id_import}),";
					}
				}
			}
		}

		$update_import = $this->update_import($id_import, 'total_2', $total);

		if($total != 0){
			$ins_data_lembur = rtrim($ins_data_lembur, ',').';';
			$this->db->query($ins_data_lembur);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return false;
			}else{
				$this->db->trans_commit();
				$return['status'] = true;
				$return['total_row'] = $total;
				$return['id_import'] = $id_import;
			}
		}else{
			$this->db->trans_commit();
			$return['status'] = true;
			$return['total_row'] = $total;
			$return['id_import'] = $id_import;
		}

		return $return;
	}

	function update_import($id_import, $total_field, $total){
		$upd[''.$total_field.''] = $total;
		$this->db->trans_begin();

		$this->db->where('h_id', $id_import);
		$this->db->update($this->h_import, $upd);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	function format_tanggal_excel($str_date){
		if(is_numeric($str_date)){
			$int_date = $str_date - 2;
			$add_date = date('Y-m-d', strtotime('1900-01-01'. ' + '.$int_date.' days'));
			$strtotime = strtotime($add_date);
			return date('Y-m-d',$strtotime);
		}else{
			return '0000-00-00';
		}
	}
}

class MyReadFilter implements IReadFilter {
    private $startRow = 0;
    private $columns = [];

    public function __construct($startRow, $columns){
        $this->startRow = $startRow;
        $this->columns = $columns;
    }

    public function readCell($column, $row, $worksheetName = ''){
        if ($row >= $this->startRow) {
            if (in_array($column, $this->columns)) {
                return true;
            }
        }
        return false;
    }
}
