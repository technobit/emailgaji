<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Detail_model extends MY_Model {

    public function list($id_import, $status_filter = "", $filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$this->db->select("*")
					->from($this->t_absensi_lembur)
					->where("id_import", $id_import);

		if(($status_filter != "")){
			$this->db->where('int_status', $status_filter);
		}
			
		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('var_nik', $filter)
					->or_like('var_nama', $filter)
					->or_like('var_bagian', $filter)
					->or_like('var_email', $filter)
					->group_end();
		}

		switch($order_by){
			case 1 : $order = 'var_nik '; break;
			case 2 : $order = 'var_nama '; break;
			case 3 : $order = 'var_bagian '; break;
			case 4 : $order = 'var_email '; break;
			default: {$order = 'int_absensi_lembur_id '; $sort = 'ASC';} break;

		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($id_import, $status_filter = "", $filter = NULL){
		$this->db->from($this->t_absensi_lembur)
				->where("id_import", $id_import);

		if(($status_filter != "")){
			$this->db->where('int_status', $status_filter);
		}

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('var_nik', $filter)
					->or_like('var_nama', $filter)
					->or_like('var_bagian', $filter)
					->or_like('var_email', $filter)
					->group_end();
		}
		return $this->db->count_all_results();
	}

	public function get_import($h_id){
		return $this->db->select("hi.*, tal.dt_periode_awal, tal.dt_periode_akhir")
					->join($this->t_absensi_lembur.' tal', 'hi.`h_id` = tal.`id_import`', 'left')
					->get_where($this->h_import.' hi', ['h_id' => $h_id])->row();
	}

	public function get_absensi_lembur($int_absensi_lembur_id){
		$rekap = $this->db->select("*")
					->get_where($this->t_absensi_lembur, ['int_absensi_lembur_id' => $int_absensi_lembur_id])->row_array();

		if(!empty($rekap)){
			$get_absensi  = $this->db->query("SELECT ha.*, mka.var_keterangan as var_deskripsi FROM {$this->h_absensi} ha
												INNER JOIN m_kode_absensi mka ON ha.var_keterangan = mka.var_kode
												WHERE ha.id_import = {$rekap['id_import']} AND ha.var_nik = {$rekap['var_nik']}")->result();
			$get_lembur  = $this->db->query("SELECT * FROM {$this->h_lembur} 
												WHERE id_import = {$rekap['id_import']} AND var_nik = {$rekap['var_nik']}")->result();
			$get_import  = $this->db->query("SELECT * FROM {$this->h_import} 
												WHERE h_id = {$rekap['id_import']}")->result();
}
		return (object) array_merge($rekap, ['absensi' => $get_absensi], ['lembur' => $get_lembur], ['import' => $get_import]);
			
	}

	public function resend($int_absensi_lembur_id){
		$upd['int_status'] = 2;
		$this->db->trans_begin();

		$this->db->where('int_absensi_lembur_id', $int_absensi_lembur_id);
		$this->db->update($this->t_absensi_lembur, $upd);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}
	
	public function resendall($id_import){
		$upd['int_status'] = 2;
		$this->db->trans_begin();

		$this->db->where('id_import', $id_import);
		$this->db->where('int_status = 4');
		$this->db->update($this->t_absensi_lembur, $upd);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}
}