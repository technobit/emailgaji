<div class="container-fluid">
    <div class="row">
        <section class="col-lg-12">
        <div class="d-print-none mb-3">
            <a href="<?=$url?>" class="btn btn-sm btn-danger tooltips"><i class="fas fa-reply"> </i> Kembali</a>
            <a href="#" onclick="window.print()" class="btn btn-sm btn-primary tooltips"><i class="fas fa-print"> </i> Print</a>
        </div>
        <div class="card card-outline">
            <div class="card-body">

            <div id="container" style="max-width:210mm;margin:auto;border:3px solid;">
                <div id="header-left" style="float:left;width: 50%;padding: 5px;">
                    <table style="width:100%;text-align: left;">
                        <tr>
                            <th colspan="3" style="padding:11px;"><span style="border-bottom: 2px solid;">REKAP KETIDAKHADIRAN & LEMBUR</span></th>
                        </tr>
                        <tr>
                            <td align="left">Nama</td>
                            <td align="left">: <?=$data->var_nama?></td>
                        </tr>
                        <tr>
                            <td align="left">NIK</td>
                            <td align="left">: <?=$data->var_nik?></td>
                        </tr>
                        <tr>
                            <td align="left">Bagian</td>
                            <td align="left">: <?=$data->var_bagian?></td>
                        </tr>
                        <tr>
                            <td align="left">Periode</td>
                            <td align="left">: <?=idn_date($data->dt_periode_awal, "j F Y")?> s.d. <?=idn_date($data->dt_periode_akhir, "j F Y")?></td>
                        </tr>
                    </table>
                </div>
                <?php foreach ($data->import as $imp){
                    $dt_batas_revisi = $imp->dt_batas_revisi;
                    $time_batas_jam_revisi = $imp->time_batas_jam_revisi;
                } ?>
                <div id="header-right" style="float:left;width: 50%;padding: 5px;">
                    <table style="width:100%;text-align: left;">
                        <tr>
                            <th colspan="3" style="text-align:right; padding: 10px"><span style="padding:10px; font-size: 16px; color:#f00; border: 2px solid #f00;">CONFIDENTIAL</span></th>
                        </tr>
                        <tr>
                            <td align="left" colspan="2">Batas Akhir Koreksi Ketidakhadiran & Lembur</td>
                        </tr>
                        <tr>
                            <td align="left">Hari</td>
                            <td align="left">: <b><?=idn_date($dt_batas_revisi, "l")?></b></td>
                        </tr>
                        <tr>
                            <td align="left">Tanggal</td>
                            <td align="left">: <b><?=idn_date($dt_batas_revisi, "j F Y")?></b></td>
                        </tr>
                        <tr>
                            <td align="left">Jam</td>
                            <td align="left">: <b><?=$time_batas_jam_revisi?></b></td>
                        </tr>
                    </table>
                </div>
                <div id="left" style="width:50%;float:left">
                    <div id="left-content" style="padding:10px;">
                        <b>REKAP KETIDAKHADIRAN</b>
                        <table style="width:100%;border: 1px solid;border-collapse: collapse;">
                            <tr style="border: 1px solid;">
                                <th style="border: 1px solid; border-collapse: collapse; text-align:center">Tanggal</th>
                                <th style="border: 1px solid; border-collapse: collapse; text-align:center">Ketidakhadiran</th>
                                <th style="border: 1px solid; border-collapse: collapse; text-align:center">Keterangan</th>
                            </tr>
                            <!--#################### Start Looping ####################-->
                            <?php foreach ($data->absensi as $abs){
                                if($abs->dt_absensi == '1970-01-01'){
                                ?>
                                    <tr style="border: 1px solid;">
                                        <td style="border: 1px solid; border-collapse: collapse; text-align: right;">-</td>
                                        <td style="border: 1px solid; border-collapse: collapse; text-align:center">-</td>
                                        <td style="border: 1px solid; border-collapse: collapse;">-</td>
                                    </tr>
                                <?php }else{ ?>
                                    <tr style="border: 1px solid;">
                                        <td style="border: 1px solid; border-collapse: collapse; text-align: right;"><?=idn_date($abs->dt_absensi, "j F Y")?></td>
                                        <td style="border: 1px solid; border-collapse: collapse; text-align:center"><?=$abs->var_keterangan?></td>
                                        <td style="border: 1px solid; border-collapse: collapse;"><?=$abs->var_deskripsi?></td>
                                    </tr>

                                <?php }
                            }?>
                            <!--#################### End Looping ####################-->
                        </table>
                    </div>
                </div>
                <div id="right" style="width:50%;float:left">
                    <div id="right-content" style="padding:10px;">
                        <b>REKAP LEMBUR</b>
                        <table style="width:100%;border: 1px solid;border-collapse: collapse;">
                            <tr style="border: 1px solid;">
                                <th style="border: 1px solid; border-collapse: collapse; text-align:center">Tanggal</th>
                                <th style="border: 1px solid; border-collapse: collapse; text-align:center">Jam Lembur</th>
                                <th style="border: 1px solid; border-collapse: collapse; text-align:center">Konversi (TUL)</th>
                            </tr>
                            <!--#################### Start Looping ####################-->
                            <?php
                            $total_jam = 0;
                            $total_konversi = 0;
                            foreach ($data->lembur as $lem){
                                $total_jam = $total_jam + $lem->dec_jam_lembur;
                                $total_konversi = $total_konversi + $lem->int_konversi;
                                if($lem->dt_lembur == '1970-01-01'){
                                ?>
                                    <tr style="border: 1px solid; text-align: center;">
                                        <td style="border: 1px solid; border-collapse: collapse;">-</td>
                                        <td style="border: 1px solid; border-collapse: collapse;">-</td>
                                        <td style="border: 1px solid; border-collapse: collapse;">-</td>
                                    </tr>
                                <?php }else{ ?>
                                    <tr style="border: 1px solid; text-align: right;">
                                        <td style="border: 1px solid; border-collapse: collapse;"><?=idn_date($lem->dt_lembur, "j F Y")?></td>
                                        <td style="border: 1px solid; border-collapse: collapse;"><?=number_format($lem->dec_jam_lembur,2, ',', '.' )?></td>
                                        <td style="border: 1px solid; border-collapse: collapse;"><?=number_format($lem->int_konversi,2, ',', '.' )?></td>
                                    </tr>
                                <?php }
                                }?>
                            <!--#################### End Looping ####################-->
                            <tr style="border: 1px solid; text-align: right;">
                                <th style="border: 1px solid; border-collapse: collapse; text-align:center">TOTAL</th>
                                <th style="border: 1px solid; border-collapse: collapse;"><?=number_format($total_jam,2, ',', '.' )?></th>
                                <th style="border: 1px solid; border-collapse: collapse;"><?=number_format($total_konversi,2, ',', '.' )?></th>
                            </tr>
                        </table>

                    </div>
                </div>
                <div id="footer" style="clear: both;margin-left:auto;margin-right: auto">
                    <ul style="list-style: number;">
                        <b>Hal yang perlu diperhatikan</b>
                        <li>Jika terdapat ketidaksesuaian tanggal atau jenis ketidakhadiran, silahkan menyerahkan form CMP (Catatan Meninggalkan Pekerjaan) ke HR untuk melakukan koreksi ketidakhadiran.</li>
                        <li>Jika terdapat ketidaksesuaian tanggal atau jam lembur, silahkan melampirkan copy SPKL ke HR untuk melakukan koreksi lembur.</li>
                    </ul>
                </div>
            </div>
            
            </div>
        </div>
        <div class="d-print-none text-right">
            <a href="<?=$url?>" class="btn btn-sm btn-danger tooltips"><i class="fas fa-reply"> </i> Kembali</a>
            <a href="#" onclick="window.print()" class="btn btn-sm btn-primary tooltips"><i class="fas fa-print"> </i> Print</a>
        </div>
        </section>
    </div>
</div>