<form method="post" action="<?=$url ?>" role="form" class="form-horizontal" id="import-form" width="80%">
<div id="modal-import" class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?=$title?></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="form-message text-center"></div>
			<div class="form-group row mb-1">
				<label for="dt_periode_awal" class="col-sm-3 col-form-label">Periode Awal</label>
				<div class="col-sm-3">
					<input type="text" id="dt_periode_awal" name="dt_periode_awal" class="form-control form-control-sm date_picker text-right" value="<?=isset($data->dt_periode_awal)? $data->dt_periode_awal : ''?>" />
				</div>
				<label for="dt_periode_akhir" class="col-sm-3 col-form-label">Periode Akhir</label>
				<div class="col-sm-3">
					<input type="text" id="dt_periode_akhir" name="dt_periode_akhir" class="form-control form-control-sm date_picker text-right" value="<?=isset($data->dt_periode_akhir)? $data->dt_periode_akhir : ''?>" />
				</div>
			</div>
			<div class="form-group row mb-1 mt-3">
				<label for="dt_batas_revisi" class="col-sm-3 col-form-label">Tangal Batas Revisi</label>
				<div class="col-sm-3">
					<input type="text" id="dt_batas_revisi" name="dt_batas_revisi" class="form-control form-control-sm date_picker text-right" value="<?=isset($data->dt_batas_revisi)? $data->dt_batas_revisi : ''?>" />
				</div>
				<label for="time_batas_jam_revisi" class="col-sm-3 col-form-label">Jam Batas Revisi</label>
				<div class="col-sm-3">
					<input type="text" id="time_batas_jam_revisi" name="time_batas_jam_revisi" class="form-control form-control-sm text-right" value="<?=isset($data->time_batas_jam_revisi)? $data->time_batas_jam_revisi : '13:00:00'?>">
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="dt_email_dikirim" class="col-sm-3 col-form-label">Tanggal Kirim</label>
				<div class="col-sm-9">
					<input type="text" id="dt_email_dikirim" name="dt_email_dikirim" class="form-control form-control-sm date_picker text-right" value="<?=isset($data->dt_email_dikirim)? $data->dt_email_dikirim : ''?>" />
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="var_subjek_email" class="col-sm-3 col-form-label">Subjek eMail</label>
				<div class="col-sm-9">
					<input type="text" id="var_subjek_email" name="var_subjek_email" class="form-control form-control-sm" value="<?=isset($data->var_subjek_email)? $data->var_subjek_email : ''?>" />
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="txt_isi_email" class="col-sm-3 col-form-label">Isi eMail</label>
				<div class="col-sm-9">
					<textarea class="form-control form-control-sm textarea" name="txt_isi_email" id="summernote" placeholder="isi email ..."><?=isset($data->txt_isi_email)? $data->txt_isi_email : ''?></textarea>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-danger">Batal</button>
			<button type="submit" class="btn btn-success">Simpan</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	$(document).ready(function(){
		$('.date_picker').daterangepicker(datepickModal);
		bsCustomFileInput.init();
		$('.currency').inputmask("numeric", {
            autoUnmask: true,
            radixPoint: ",",
            groupSeparator: ".",
            digits: 2,
            autoGroup: true,
            prefix: '',
            rightAlign: true,
            positionCaretOnClick: true,
        });
		$('#summernote').summernote({
			dialogsInBody: true,
			height: 250,                 // set editor height
			minHeight: null,             // set minimum height of editor
			maxHeight: null,             // set maximum height of editor
			focus: true                  // set focus to editable area after initializing summernote
		});
		$("#import-form").validate({
			rules: {
				dt_email_dikirim:{
			        required: true
				},
				var_subjek_email:{
			        required: true,
					minlength: 5
				},
				txt_isi_email:{
			        required: true,
					minlength: 10
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
                let blc = '#modal-import';
                blockUI(blc);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
						setFormMessage('.form-message', data);
						if(data.stat){
							dataTable.draw();
							resetForm(form)
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});
	});
</script>