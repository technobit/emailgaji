<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['absen_lembur']['get']                       = 'absen_lembur/absen_lembur';
$route['absen_lembur']['post']                      = 'absen_lembur/absen_lembur/list';
$route['absen_lembur/add']['get']                   = 'absen_lembur/absen_lembur/add';
$route['absen_lembur/save']['post']                 = 'absen_lembur/absen_lembur/save';
$route['absen_lembur/([a-zA-Z0-9]+)']['get']        = 'absen_lembur/absen_lembur/get/$1';
$route['absen_lembur/([a-zA-Z0-9]+)']['post']       = 'absen_lembur/absen_lembur/update/$1';
$route['absen_lembur/([a-zA-Z0-9]+)/del']['get']    = 'absen_lembur/absen_lembur/confirm/$1';
$route['absen_lembur/([a-zA-Z0-9]+)/del']['post']   = 'absen_lembur/absen_lembur/delete/$1';

$route['detail_absen_lembur/([a-zA-Z0-9]+)']['get']         = 'absen_lembur/detail/index/$1';
$route['detail_absen_lembur/([a-zA-Z0-9]+)']['post']        = 'absen_lembur/detail/list/$1';
$route['detail_absen_lembur/([a-zA-Z0-9]+)/rekap']['get']    = 'absen_lembur/detail/get_rekap/$1';
$route['detail_absen_lembur/([a-zA-Z0-9]+)/resend']['get']  = 'absen_lembur/detail/confirm/$1';
$route['detail_absen_lembur/([a-zA-Z0-9]+)/resend']['post'] = 'absen_lembur/detail/resend/$1';
$route['detail_absen_lembur/resendall/([a-zA-Z0-9]+)']['get']   = 'absen_lembur/detail/confirm_resendall/$1';
$route['detail_absen_lembur/resendall/([a-zA-Z0-9]+)']['post']  = 'absen_lembur/detail/resendall/$1';
$route['detail_absen_lembur/detail/([a-zA-Z0-9]+)']['post']     = 'absen_lembur/detail/export/$1';
