<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Email_model extends MY_Model {

	public function get(){
		return $this->db->select("*")
                        ->get_where($this->m_email_config, ['int_config_id' => 1])->row();
	}
	 
    public function update($in){
	    $this->db->where('int_config_id', $in['int_config_id'])
                    ->update($this->m_email_config, ['var_nama_config' => $in['var_nama_config'],
                                            'var_host' => $in['var_host'],
                                            'var_email' => $in['var_email'],
                                            'var_pass' => $in['var_pass'],
                                            'var_secure' => $in['var_secure'],
                                            'int_port' => $in['int_port'],
                                            ]);
    }
}