<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Absensi_model extends MY_Model {

	public function list($filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$order_by   = strtolower($order_by); 
		$sort       = (strtolower(trim($sort)) == 'asc')? 'ASC' : 'DESC';

		$this->db->select("*")
					->from($this->m_kode_absensi);

		if(!empty($filter)){ // filters 
			$filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('var_kode', $filter)
					->or_like('var_keterangan', $filter)
					->group_end();
		}

		$order = 'int_ref_id ';
		switch($order_by){
			case 1 : $order = 'var_kode '; break;
			case 2 : $order = 'var_keterangan '; break;
			default : $order = 'int_kode_absensi_id '; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($filter = NULL){
		$this->db->from($this->m_kode_absensi);

		if(!empty($filter)){ // filters 
			$filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
			->like('var_kode', $filter)
			->or_like('var_keterangan', $filter)
			->group_end();
}
		return $this->db->count_all_results();
	}

	public function create($in){
		$col['var_kode']		= $in['var_kode'];
        $col['var_keterangan']	= $in['var_keterangan'];

        $this->db->trans_begin();
		$this->db->insert($this->m_kode_absensi, $col);
		$user_id = $this->db->insert_id();
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}
	
	public function get($int_kode_absensi_id){
		$data =  $this->db->query("	SELECT *
									FROM	{$this->m_kode_absensi} 
									WHERE	int_kode_absensi_id = '{$int_kode_absensi_id}'")->row_array();

		return (object) $data;									
	}

	public function update($int_kode_absensi_id, $in){
        $col['var_keterangan']	= $in['var_keterangan'];
        $col['var_kode']		= $in['var_kode'];

		$this->db->trans_begin();

		$this->db->where('int_kode_absensi_id', $int_kode_absensi_id);
		$this->db->update($this->m_kode_absensi, $col);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}
	public function delete($int_kode_absensi_id){
		$this->db->trans_begin();
		$this->db->query("DELETE FROM {$this->m_kode_absensi} WHERE int_kode_absensi_id = ?", [$int_kode_absensi_id]);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}
}
