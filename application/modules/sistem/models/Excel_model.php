<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Excel_model extends MY_Model {

	public function list($filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$order_by   = strtolower($order_by); 
		$sort       = (strtolower(trim($sort)) == 'asc')? 'ASC' : 'DESC';

		$this->db->select("*")
					->from($this->m_gaji_ref);

		if(!empty($filter)){ // filters 
			$filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('var_nama_kolom', $filter)
					->group_end();
		}

		$order = 'int_ref_id ';
		switch($order_by){
			case 1 : $order = 'var_kode_kolom '; break;
			case 2 : $order = 'var_nama_kolom '; break;
			default : $order = 'int_ref_id '; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($filter = NULL){
		$this->db->from($this->m_gaji_ref);

		if(!empty($filter)){ // filters 
			$filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
			->like('var_nama_kolom', $filter)
			->group_end();
}
		return $this->db->count_all_results();
	}

	public function get($var_kode_kolom){
		$data =  $this->db->query("	SELECT *
									FROM	{$this->m_gaji_ref} 
									WHERE	var_kode_kolom = '{$var_kode_kolom}'")->row_array();

		return (object) $data;									
	}

	public function update($var_kode_kolom, $in){
        $col['var_nama_kolom']		= $in['var_nama_kolom'];
        $col['var_kode_kolom']		= $in['var_kode_kolom'];


		$this->db->trans_begin();

		$this->db->where('var_kode_kolom', $var_kode_kolom);
		$this->db->update($this->m_gaji_ref, $col);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

}
