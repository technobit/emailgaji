<script>
    function exportData(th){
        $('.form-message').html('');
        let blc = $(th).data('block');
        blockUI(blc);
        $.AjaxDownloader({
            url  : $(th).data('url'),
            data : {
                <?php echo $page->tokenName ?> : $('meta[name=<?php echo $page->tokenName ?>]').attr("content"),
            }
        });
        setTimeout(function(){unblockUI(blc)}, 3000);
    }

    var dataTable;
    $(document).ready(function() {
        
        dataTable = $('#table_data').DataTable({
            "bFilter": true,
            "bServerSide": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "pageLength": 75,
            "lengthMenu": [[50, 75, 100, -1], [50, 75, 100, "All"]],
            "ajax": {
                "url": "<?=site_url("{$routeURL}") ?>",
                "dataType": "json",
                "type": "POST",
                "data": function(d) {
                    d.<?=$page->tokenName ?> = $('meta[name=<?=$page->tokenName ?>]').attr("content");
                },
                "dataSrc": function(json) {
                    if (json.<?=$page->tokenName ?> !== undefined) $('meta[name=<?=$page->tokenName ?>]').attr("content", json.<?=$page->tokenName ?>);
                    return json.aaData;
                }
            },
            "aoColumns": [{
                    "sWidth": "20",
                    "sClass": "text-right",
                    "bSearchable": false
                },
                {
                    "sWidth": "30"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "10",
                    "sClass": "text-right",
                    "bSortable": false,
                    "bSearchable": false,
                }
            ]
        });

        $('.dataTables_filter input')
            .unbind()
            .bind('keyup', function(e) {
                if (e.keyCode == 13) {
                    dataTable.search($(this).val()).draw();
                }
            });
    });
</script>