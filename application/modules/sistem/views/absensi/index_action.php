<form method="post" action="<?=$url?>" role="form" class="form-horizontal" id="excel-form" width="80%">
<div id="modal-excel" class="modal-dialog modal-md" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?=$title?></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="form-message text-center"></div>
			<div class="form-group row mb-1">
				<label for="var_kode" class="col-sm-3 col-form-label">Kode</label>
				<div class="col-sm-9">
					<input type="text" class="form-control form-control-sm" id="var_kode" placeholder="Kode" name="var_kode" value="<?=isset($data->var_kode)? $data->var_kode : ''?>"/>
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="var_keterangan" class="col-sm-3 col-form-label">Keterangan</label>
				<div class="col-sm-9">
					<input type="text" class="form-control form-control-sm" id="var_keterangan" placeholder="Keterangan" name="var_keterangan" value="<?=isset($data->var_keterangan)? $data->var_keterangan : ''?>"/>
				</div>
			</div>
			<input type="hidden" id="int_kode_absensi_id " name="int_kode_absensi_id" value="<?=isset($data->int_kode_absensi_id)? $data->int_kode_absensi_id : ''?>"/>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-danger">Keluar</button>
			<button type="submit" class="btn btn-success">Simpan</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>

	$(document).ready(function(){
		$("#excel-form").validate({
			rules: {
				var_kode: {
					required: true
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
				blockUI(form);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
						unblockUI(form);
						setFormMessage('.form-message', data);
						if(data.stat){
							resetForm('#excel-form');
							dataTable.draw();
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});
	});
</script>