<script>
    $(document).ready(function(){
        $('.currency').inputmask("numeric", {
            autoUnmask: true,
            radixPoint: ",",
            groupSeparator: ".",
            digits: 2,
            autoGroup: true,
            prefix: '',
            rightAlign: true,
            positionCaretOnClick: true,
        });
        $("#email-form").validate({
            rules: {
                var_nama_config: {
                    required: true,
                    maxlength: 30
                },
                var_host: {
                    required: true,
                }
                var_email: {
                    required: true,
                }
                var_pass: {
                    required: true,
                }
                var_secure: {
                    required: true,
                }
                int_port: {
                    required: true,
                    digits:true
                }
            },
            validClass: "valid-feedback",
            errorElement: "div", // contain the error msg in a small tag
            errorClass: 'invalid-feedback',
            errorPlacement: erp,
            highlight: hl,
            unhighlight: uhl,
            success: sc
        });

    });
</script>