<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Excel extends MX_Controller {

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'EXCEL'; // kode menu pada tabel menu, 1 menu : 1 controller
		$this->module 	= 'sistem';
		$this->routeURL	= 's_excel';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('excel_model', 'user');
		$this->load->model('group_model', 'group');
    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Konfigurasi Excel';
		$this->page->menu 	  = 'sistem';
		$this->page->submenu1 = 's_excel';
		$this->breadcrumb->title = 'Konfigurasi Excel';
		$this->breadcrumb->icon = 'fas fa-file-excel';
		$this->breadcrumb->list = ['Sistem', 'Konfigurasi Excel'];
		$this->js = true;
		$data['url'] = site_url("{$this->routeURL}/add");
		$data['url_export'] = site_url("export/{$this->routeURL}");
		$this->render_view('excel/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$total = $this->user->listCount($this->input->post('search[value]', TRUE));
		$ldata = $this->user->list($this->input->post('search[value]', TRUE), $this->input->post('order[0][column]', true), $this->input->post('order[0][dir]'), $this->input->post('length', true), $this->input->post('start', true));

		$i 	   = $this->input->post('start', true);
		foreach($ldata as $d){
			$edit = '';
			if($d->is_edit == 1){
				$edit = '<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}").'/'.$d->var_kode_kolom.'" class="ajax_modal btn btn-xs btn-warning tooltips" data-placement="top" data-original-title="Edit" ><i class="fa fa-edit"></i></a>';
			}
			$data[] = array($d->int_ref_id.'. ', strtoupper($d->var_kode_kolom), $d->var_nama_kolom, $edit);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()));
	}
	
	public function get($var_kode_kolom){
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->user->get($var_kode_kolom);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Terjadi Kesalahan.', 'message' => 'Data yang dicari tidak ditemukan. ']], true);
		}else{
			$data['data'] 	= $res;
			$data['url']	= site_url("{$this->routeURL}/$var_kode_kolom");
			$data['title']	= 'Edit Kolom Excel';
			$this->load_view('excel/index_action', $data);
		}
		
	}

	public function update($var_kode_kolom){
		$this->authCheckDetailAccess('u');

        $this->form_validation->set_rules('var_nama_kolom', 'Nama Kolom', "required|is_unique_update[{$this->user->m_gaji_ref}.var_nama_kolom.var_kode_kolom.{$var_kode_kolom}]|min_length[1]");

		
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false, 
								'msg' => "Terjadi kesalahan",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
            $check = $this->user->update($var_kode_kolom, $this->input->post());
			$this->set_json([  'stat' => $check, 
								'mc' => $check, //modal close
								'msg' => ($check)? "Data berhasil di-update" : "Terjadi kesalahan teknis",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);

        }
	}

	public function export(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page
		
		$ldata = $this->user->list($this->input->post('search[value]', TRUE), $this->input->post('order[0][column]', true), $this->input->post('order[0][dir]'), $this->input->post('length', true), $this->input->post('start', true));

        $title    = 'Template Gaji';

        $filename = 'Template Gaji - '.idn_date($detail->dt_periode, "F Y");


		$input_file = 'assets/export/template_gaji.xlsx';
		/** Load $inputFileName to a Spreadsheet object **/
		//$spreadsheet = new Spreadsheet();
		$spreadsheet = PhpOffice\PhpSpreadsheet\IOFactory::load($input_file);
		$spreadsheet->setActiveSheetIndex(0)
					->setCellValue('A1', 'Daftar Penerima Gaji Periode '.idn_date($detail->dt_periode, "F Y"));

		$sheet = $spreadsheet->getActiveSheet();

        $i = 0;
        $x = 3;
        foreach($ldata as $d){
            $i++;
			$x++;

			//$data[] = array($d->int_ref_id.'. ', strtoupper($d->var_kode_kolom), $d->var_nama_kolom, $edit);

            $sheet->setCellValue(strtoupper($d->var_kode_kolom).'1', $d->var_nama_kolom);
			/*$sheet->setCellValueExplicit('B'.$x, $d->a,\PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $sheet->setCellValue('C'.$x, $d->b);
            $sheet->setCellValue('D'.$x, $d->d);
            $sheet->setCellValue('E'.$x, $d->bv);
            $sheet->setCellValue('F'.$x, $status);*/
        }
		
		$sheet->setTitle($title);
        $spreadsheet->setActiveSheetIndex(0);

        $this->set_header($filename);

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		if (ob_get_contents()) ob_end_clean();
		$writer->save('php://output');
		exit;
	}
}
