<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Absensi extends MX_Controller {

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'ABSENSI'; // kode menu pada tabel menu, 1 menu : 1 controller
		$this->module 	= 'sistem';
		$this->routeURL	= 's_absensi';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('absensi_model', 'model');
    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Konfigurasi Keterangan Absensi';
		$this->page->menu 	  = 'sistem';
		$this->page->submenu1 = 's_absensi';
		$this->breadcrumb->title = 'Konfigurasi Keterangan Absensi';
		$this->breadcrumb->icon = 'fas fa-calendar-check';
		$this->breadcrumb->list = ['Sistem', 'Konfigurasi Keterangan Absensi'];
		$this->js = true;
		$data['url'] = site_url("{$this->routeURL}/add");
		$this->render_view('absensi/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$total = $this->model->listCount($this->input->post('search[value]', TRUE));
		$ldata = $this->model->list($this->input->post('search[value]', TRUE), $this->input->post('order[0][column]', true), $this->input->post('order[0][dir]'), $this->input->post('length', true), $this->input->post('start', true));

		$i 	   = $this->input->post('start', true);
		foreach($ldata as $d){
			$action = '<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}").'/'.$d->int_kode_absensi_id.'" class="ajax_modal btn btn-xs btn-warning tooltips" data-placement="top" data-original-title="Edit" ><i class="fa fa-edit"></i></a>
			<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}").'/'.$d->int_kode_absensi_id.'/del" class="ajax_modal btn btn-xs btn-danger tooltips" data-placement="top" data-original-title="Delete" ><i class="fa fa-trash"></i></a>';
			$i++;
			$data[] = array($i.'. ', strtoupper($d->var_kode), $d->var_keterangan, $action);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()));
	}
	
	public function add(){
		if($this->authCheckDetailAccess('c', true) == false) return; // hak akses untuk modal popup

		$data['url']   = site_url("{$this->routeURL}/save");
		$data['title'] = 'Tambah Keterangan Absensi';
		$this->load_view('absensi/index_action', $data, true);
	}
	
	public function save(){
		$this->authCheckDetailAccess('c');

		$this->form_validation->set_rules('var_kode', 'Kode', 'required');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'mc' => false, //modal close
								'msg' => "Terjadi kesalahan",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
            $query = $this->model->create($this->input->post());
			$this->set_json([  'stat' => true, 
								'mc' => $query, //modal close
								'msg' => "Data berhasil dibuat",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);

        }
	}
	
	public function get($int_kode_absensi_id){
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_kode_absensi_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Terjadi Kesalahan.', 'message' => 'Data yang dicari tidak ditemukan. ']], true);
		}else{
			$data['data'] 	= $res;
			$data['url']	= site_url("{$this->routeURL}/$int_kode_absensi_id");
			$data['title']	= 'Edit Kode Absensi';
			$this->load_view('absensi/index_action', $data);
		}
		
	}

	public function update($int_kode_absensi_id){
		$this->authCheckDetailAccess('u');

        $this->form_validation->set_rules('var_kode', 'Kode', "required");

		
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false, 
								'msg' => "Terjadi kesalahan",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
            $check = $this->model->update($int_kode_absensi_id, $this->input->post());
			$this->set_json([  'stat' => $check, 
								'mc' => $check, //modal close
								'msg' => ($check)? "Data berhasil di-update" : "Terjadi kesalahan teknis",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);

        }
	}
	public function confirm($int_kode_absensi_id){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_kode_absensi_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Terjadi Kesalahan.', 'message' => 'Data yang dicari tidak ditemukan. ']], true);
		}else{
			$data['url']	= site_url("{$this->routeURL}/$int_kode_absensi_id/del");
			$data['title']	= 'Hapus Kode Absensi';
			$data['info']	= [	'Kode' => $res->var_kode, 
								'Keterangan' => $res->var_keterangan];
			$this->load_view('absensi/index_delete', $data);
		}
	}

	public function delete($int_kode_absensi_id){
		$this->authCheckDetailAccess('d');

		$check = $this->model->delete($int_kode_absensi_id);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Data berhasil dihapus" : "Data tidak dapat dihapus",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
		
	}
}
