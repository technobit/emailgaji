<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends MX_Controller {
	function __construct(){
        parent::__construct();
		
		$this->kodeMenu = 'EMAIL';
        $this->module   = 'sistem';
        $this->routeURL = 's_email';
		$this->authCheck();

        $this->load->library('form_validation');
        $this->form_validation->CI =& $this;

		$this->load->model('email_model', 'model');
    }
	
	public function index(){
        $this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Konfikurasi Email';
		$this->page->menu 	  = 'sistem';
		$this->page->submenu1 = 's_email';
		$this->breadcrumb->title = 'Konfigurasi Email Server';
		$this->breadcrumb->list = ['Sistem', 'Konfigurasi Email'];
		$this->breadcrumb->icon = 'fas fa-server';
		$this->js = true;
        $data['data']        = $this->model->get();
        $data['url_update']     = site_url("{$this->routeURL}");
		$this->render_view('email/index', $data, false);
	}

    public function update(){
        $this->authCheckDetailAccess('u'); // hak akses untuk render page

        $this->form_validation->set_rules('var_nama_config', 'Nama Server', 'required');

        if($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('stat', false);
            $this->session->set_flashdata('msg', 'Konfigurasi email server gagal disimpan. '.validation_errors('<br>', ' '));
        } else {
            $this->model->update($this->input_post());

            $this->session->set_flashdata('stat', true);
            $this->session->set_flashdata('msg', 'Konfigurasi email server berhasil disimpan.');
        }

        redirect('s_email');
    }

}
