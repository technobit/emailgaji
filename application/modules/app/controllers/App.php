<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends MX_Controller {
    private $mode = ['Hari Ini', 'Minggu Ini', 'Bulan Ini'];
	function __construct(){
        parent::__construct();
		
		$this->kodeMenu = 'APP-HOME';
		$this->authCheck();
		
		$this->load->model('app_model', 'model');
    }
	
	public function index(){
		$this->page->subtitle = 'Dashboard';
		$this->page->menu = 'dashboard';
        $this->breadcrumb->title = 'Dashboard';;
        $this->breadcrumb->list = [getServerDate(true)];
        $this->js = true;
        
        $last_import_gaji = $this->model->get_last_import(0); //gaji
        if($last_import_gaji){
            $last_success_gaji = $this->model->get_last_data_gaji($last_import_gaji->h_id, 3);
            $last_progress_gaji = $this->model->get_last_data_gaji($last_import_gaji->h_id, 2);
            $last_failed_gaji = $this->model->get_last_data_gaji($last_import_gaji->h_id, 4);

            $data['last_import_gaji']  = $last_import_gaji->total;
            $data['last_success_gaji']  = $last_success_gaji->total;
            $data['last_progress_gaji']  = $last_progress_gaji->total;
            $data['last_failed_gaji']  = $last_failed_gaji->total;
        }else{
            $data['last_import_gaji']  = 0;
            $data['last_success_gaji']  = 0;
            $data['last_progress_gaji']  = 0;
            $data['last_failed_gaji']  = 0;
        }

        $last_import_absen_lembur = $this->model->get_last_import(2); //absensi & lembur
        if($last_import_absen_lembur){
            $last_total_absen_lembur = $this->model->get_last_data_absen_lembur($last_import_absen_lembur->h_id);
            $last_success_absen_lembur = $this->model->get_last_data_absen_lembur($last_import_absen_lembur->h_id, 3);
            $last_progress_absen_lembur = $this->model->get_last_data_absen_lembur($last_import_absen_lembur->h_id, 2);
            $last_failed_absen_lembur = $this->model->get_last_data_absen_lembur($last_import_absen_lembur->h_id, 4);

            $data['last_import_absen_lembur']  = $last_total_absen_lembur->total;
            $data['last_success_absen_lembur']  = $last_success_absen_lembur->total;
            $data['last_progress_absen_lembur']  = $last_progress_absen_lembur->total;
            $data['last_failed_absen_lembur']  = $last_failed_absen_lembur->total;
        }else{
            $data['last_import_absen_lembur']  = 0;
            $data['last_success_absen_lembur']  = 0;
            $data['last_progress_absen_lembur']  = 0;
            $data['last_failed_absen_lembur']  = 0;
        }

        $last_import_pajak_bulanan = $this->model->get_last_import(3); //pajak_bulanan bulanan
        if($last_import_pajak_bulanan){
            $last_success_pajak_bulanan = $this->model->get_last_data_pajak_bulanan($last_import_pajak_bulanan->h_id, 3);
            $last_progress_pajak_bulanan = $this->model->get_last_data_pajak_bulanan($last_import_pajak_bulanan->h_id, 2);
            $last_failed_pajak_bulanan = $this->model->get_last_data_pajak_bulanan($last_import_pajak_bulanan->h_id, 4);

            $data['last_import_pajak_bulanan']  = $last_import_pajak_bulanan->total;
            $data['last_success_pajak_bulanan']  = $last_success_pajak_bulanan->total;
            $data['last_progress_pajak_bulanan']  = $last_progress_pajak_bulanan->total;
            $data['last_failed_pajak_bulanan']  = $last_failed_pajak_bulanan->total;
        }else{
            $data['last_import_pajak_bulanan']  = 0;
            $data['last_success_pajak_bulanan']  = 0;
            $data['last_progress_pajak_bulanan']  = 0;
            $data['last_failed_pajak_bulanan']  = 0;
        }

        $last_import_pajak_tahunan = $this->model->get_last_import(4); //pajak_tahunan tahunan
        if($last_import_pajak_tahunan){
            $last_success_pajak_tahunan = $this->model->get_last_data_pajak_tahunan($last_import_pajak_tahunan->h_id, 3);
            $last_progress_pajak_tahunan = $this->model->get_last_data_pajak_tahunan($last_import_pajak_tahunan->h_id, 2);
            $last_failed_pajak_tahunan = $this->model->get_last_data_pajak_tahunan($last_import_pajak_tahunan->h_id, 4);

            $data['last_import_pajak_tahunan']  = $last_import_pajak_tahunan->total;
            $data['last_success_pajak_tahunan']  = $last_success_pajak_tahunan->total;
            $data['last_progress_pajak_tahunan']  = $last_progress_pajak_tahunan->total;
            $data['last_failed_pajak_tahunan']  = $last_failed_pajak_tahunan->total;
        }else{
            $data['last_import_pajak_tahunan']  = 0;
            $data['last_success_pajak_tahunan']  = 0;
            $data['last_progress_pajak_tahunan']  = 0;
            $data['last_failed_pajak_tahunan']  = 0;
        }

        $last_import_pajak = $this->model->get_last_import(1); //pajak tahunan lama
        if($last_import_pajak){
            $last_success_pajak = $this->model->get_last_data_pajak($last_import_pajak->h_id, 3);
            $last_progress_pajak = $this->model->get_last_data_pajak($last_import_pajak->h_id, 2);
            $last_failed_pajak = $this->model->get_last_data_pajak($last_import_pajak->h_id, 4);

            $data['last_import_pajak']  = $last_import_pajak->total;
            $data['last_success_pajak']  = $last_success_pajak->total;
            $data['last_progress_pajak']  = $last_progress_pajak->total;
            $data['last_failed_pajak']  = $last_failed_pajak->total;
        }else{
            $data['last_import_pajak']  = 0;
            $data['last_success_pajak']  = 0;
            $data['last_progress_pajak']  = 0;
            $data['last_failed_pajak']  = 0;
        }

        $this->render_view('index', $data, false);
	}
}
