<div class="container-fluid">

    <div class="card card-outline card-danger">
        <div class="card-header">
            <h5 class="card-title">
                <i class="fas fa-money-bill-wave"></i>
                Rekapitulasi Penggajian Terakhir
            </h5>
        </div>
        <div class="card-body">

            <div class="row">
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="info-box bg-info">
                        <span class="info-box-icon"><i class="fas fa-money-bill-wave"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Data Gaji</span>
                            <span class="info-box-number"><?=$last_import_gaji?></span>
                        <div class="progress">
                            <div class="progress-bar" style="width: 100%"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 col-12">
                    <div class="info-box bg-success">
                        <span class="info-box-icon"><i class="fas fa-check-double"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Email Terkirim</span>
                            <span class="info-box-number"><?=$last_success_gaji?></span>
                        <div class="progress">
                            <?php $progress = $last_success_gaji/$last_import_gaji*100 ?>
                            <div class="progress-bar" style="width: <?=$progress?>%"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 col-12">
                    <div class="info-box bg-orange">
                        <span class="info-box-icon"><i class="fas fa-hourglass-half"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Email Dalam Proses</span>
                            <span class="info-box-number"><?=$last_progress_gaji?></span>
                        <div class="progress">
                            <?php $progress = $last_progress_gaji/$last_import_gaji*100 ?>
                            <div class="progress-bar" style="width: <?=$progress?>%"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 col-12">
                    <div class="info-box bg-danger">
                        <span class="info-box-icon"><i class="fas fa-exclamation-triangle"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Email Gagal Terkirim</span>
                            <span class="info-box-number"><?=$last_failed_gaji?></span>
                        <div class="progress">
                            <?php $progress = $last_failed_gaji/$last_import_gaji*100 ?>
                            <div class="progress-bar" style="width: <?=$progress?>%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    
    <div class="card card-outline card-warning">
        <div class="card-header">
            <h5 class="card-title">
                <i class="fas fa-clock"></i>
                Rekapitulasi Absensi & Lembur Terakhir
            </h5>
        </div>
        <div class="card-body">

            <div class="row">
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="info-box bg-info">
                        <span class="info-box-icon"><i class="fas fa-clock"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Data Absensi & Lembur</span>
                            <span class="info-box-number"><?=$last_import_absen_lembur?></span>
                            <div class="progress">
                                <div class="progress-bar" style="width: 100%"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 col-12">
                    <div class="info-box bg-success">
                        <span class="info-box-icon"><i class="fas fa-check-double"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Email Terkirim</span>
                            <span class="info-box-number"><?=$last_success_absen_lembur?></span>
                            <div class="progress">
                                <?php $progress = $last_success_absen_lembur/$last_import_absen_lembur*100 ?>
                                <div class="progress-bar" style="width: <?=$progress?>%"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 col-12">
                    <div class="info-box bg-orange">
                        <span class="info-box-icon"><i class="fas fa-hourglass-half"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Email Dalam Proses</span>
                            <span class="info-box-number"><?=$last_progress_absen_lembur?></span>
                        <div class="progress">
                                <?php $progress = $last_progress_absen_lembur/$last_import_absen_lembur*100 ?>
                                <div class="progress-bar" style="width: <?=$progress?>%"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 col-12">
                    <div class="info-box bg-danger">
                        <span class="info-box-icon"><i class="fas fa-exclamation-triangle"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Email Gagal Terkirim</span>
                            <span class="info-box-number"><?=$last_failed_absen_lembur?></span>
                        <div class="progress">
                                <?php $progress = $last_failed_absen_lembur/$last_import_absen_lembur*100 ?>
                                <div class="progress-bar" style="width: <?=$progress?>%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="card card-outline card-primary">
        <div class="card-header">
            <h5 class="card-title">
                <i class="fas fa-calculator"></i>
                Rekapitulasi Pengiriman Bukti Potongan Pajak Bulanan
            </h5>
        </div>
        <div class="card-body">

        <div class="row">
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="info-box bg-info">
                        <span class="info-box-icon"><i class="fas fa-calculator"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Bukti Potongan Pajak</span>
                            <span class="info-box-number"><?=$last_import_pajak_bulanan?></span>
                        <div class="progress">
                            <div class="progress-bar" style="width: 100%"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 col-12">
                    <div class="info-box bg-success">
                        <span class="info-box-icon"><i class="fas fa-check-double"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Email Terkirim</span>
                            <span class="info-box-number"><?=$last_success_pajak_bulanan?></span>
                        <div class="progress">
                            <?php $progress = $last_success_pajak_bulanan/$last_import_pajak_bulanan*100 ?>
                            <div class="progress-bar" style="width: <?=$progress?>%"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 col-12">
                    <div class="info-box bg-orange">
                        <span class="info-box-icon"><i class="fas fa-hourglass-half"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Email Dalam Proses</span>
                            <span class="info-box-number"><?=$last_progress_pajak_bulanan?></span>
                        <div class="progress">
                            <?php $progress = $last_progress_pajak_bulanan/$last_import_pajak_bulanan*100 ?>
                            <div class="progress-bar" style="width: <?=$progress?>%"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 col-12">
                    <div class="info-box bg-danger">
                        <span class="info-box-icon"><i class="fas fa-exclamation-triangle"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Email Gagal Terkirim</span>
                            <span class="info-box-number"><?=$last_failed_pajak_bulanan?></span>
                        <div class="progress">
                            <?php $progress = $last_failed_pajak_bulanan/$last_import_pajak_bulanan*100 ?>
                            <div class="progress-bar" style="width: <?=$progress?>%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="card card-outline card-primary">
        <div class="card-header">
            <h5 class="card-title">
                <i class="fas fa-calculator"></i>
                Rekapitulasi Pengiriman Bukti Potongan Pajak Tahunan
            </h5>
        </div>
        <div class="card-body">

        <div class="row">
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="info-box bg-info">
                        <span class="info-box-icon"><i class="fas fa-calculator"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Bukti Potongan Pajak</span>
                            <span class="info-box-number"><?=$last_import_pajak_tahunan?></span>
                        <div class="progress">
                            <div class="progress-bar" style="width: 100%"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 col-12">
                    <div class="info-box bg-success">
                        <span class="info-box-icon"><i class="fas fa-check-double"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Email Terkirim</span>
                            <span class="info-box-number"><?=$last_success_pajak_tahunan?></span>
                        <div class="progress">
                            <?php $progress = $last_success_pajak_tahunan/$last_import_pajak_tahunan*100 ?>
                            <div class="progress-bar" style="width: <?=$progress?>%"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 col-12">
                    <div class="info-box bg-orange">
                        <span class="info-box-icon"><i class="fas fa-hourglass-half"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Email Dalam Proses</span>
                            <span class="info-box-number"><?=$last_progress_pajak_tahunan?></span>
                        <div class="progress">
                            <?php $progress = $last_progress_pajak_tahunan/$last_import_pajak_tahunan*100 ?>
                            <div class="progress-bar" style="width: <?=$progress?>%"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 col-12">
                    <div class="info-box bg-danger">
                        <span class="info-box-icon"><i class="fas fa-exclamation-triangle"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Email Gagal Terkirim</span>
                            <span class="info-box-number"><?=$last_failed_pajak_tahunan?></span>
                        <div class="progress">
                            <?php $progress = $last_failed_pajak_tahunan/$last_import_pajak_tahunan*100 ?>
                            <div class="progress-bar" style="width: <?=$progress?>%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <div class="card card-outline card-primary">
        <div class="card-header">
            <h5 class="card-title">
                <i class="fas fa-calculator"></i>
                Rekapitulasi Pengiriman Bukti Potongan Pajak Tahunan (Lama)
            </h5>
        </div>
        <div class="card-body">

            <div class="row">
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="info-box bg-info">
                        <span class="info-box-icon"><i class="fas fa-calculator"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Bukti Potongan Pajak</span>
                            <span class="info-box-number"><?=$last_import_pajak?></span>
                        <div class="progress">
                            <div class="progress-bar" style="width: 100%"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 col-12">
                    <div class="info-box bg-success">
                        <span class="info-box-icon"><i class="fas fa-check-double"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Email Terkirim</span>
                            <span class="info-box-number"><?=$last_success_pajak?></span>
                        <div class="progress">
                            <?php $progress = $last_success_pajak/$last_import_pajak*100 ?>
                            <div class="progress-bar" style="width: <?=$progress?>%"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 col-12">
                    <div class="info-box bg-orange">
                        <span class="info-box-icon"><i class="fas fa-hourglass-half"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Email Dalam Proses</span>
                            <span class="info-box-number"><?=$last_progress_pajak?></span>
                        <div class="progress">
                            <?php $progress = $last_progress_pajak/$last_import_pajak*100 ?>
                            <div class="progress-bar" style="width: <?=$progress?>%"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 col-12">
                    <div class="info-box bg-danger">
                        <span class="info-box-icon"><i class="fas fa-exclamation-triangle"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Email Gagal Terkirim</span>
                            <span class="info-box-number"><?=$last_failed_pajak?></span>
                        <div class="progress">
                            <?php $progress = $last_failed_pajak/$last_import_pajak*100 ?>
                            <div class="progress-bar" style="width: <?=$progress?>%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>