<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class App_model extends MY_Model {

    public function get_last_data_gaji($id_import, $int_status){
	    return $this->db->query("SELECT COUNT(*) as total FROM {$this->t_gaji}
                                WHERE int_status = $int_status AND id_import = $id_import")->row();
    }

    public function get_last_data_absen_lembur($id_import, $int_status = ''){

        if($int_status == ''){
            $where =  "WHERE id_import = $id_import";
        }else{
            $where = "WHERE int_status = $int_status AND id_import = $id_import";
        }
        
        return $this->db->query("SELECT COUNT(*) as total FROM {$this->t_absensi_lembur} {$where}")->row();
    }

    public function get_last_data_pajak_bulanan($id_import, $int_status){
	    return $this->db->query("SELECT COUNT(*) as total FROM {$this->t_potongan_pajak_bulanan}
                                WHERE int_status = $int_status AND id_import = $id_import")->row();
    }

    public function get_last_data_pajak_tahunan($id_import, $int_status){
	    return $this->db->query("SELECT COUNT(*) as total FROM {$this->t_potongan_pajak_2024}
                                WHERE int_status = $int_status AND id_import = $id_import")->row();
    }

    public function get_last_data_pajak($id_import, $int_status){
	    return $this->db->query("SELECT COUNT(*) as total FROM {$this->t_potongan_pajak}
                                WHERE int_status = $int_status AND id_import = $id_import")->row();
    }

    public function get_last_import($int_type){
        $level = $this->session->userdata['int_level'];

        if($level == 2){
            $where =  "WHERE int_level = {$level} AND int_type = {$int_type}";
        }else{
            $where = "WHERE int_type = {$int_type}";
        }
	    return $this->db->query("SELECT * FROM {$this->h_import}
        {$where}
        ORDER BY import_date DESC LIMIT 1")->row();
    }
}