<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication 
{
    private $ci;
    
	function __construct(){
		$this->ci =& get_instance();
	}
    public function authenticate($username = '', $password = ''){
		$this->ci->load->model('app/auth_model', 'auth');
		$check = $this->ci->auth->getAuthCredential($username, $password);
        x_debug($this->ci->auth->getUserCredential());
		return ($check)? ['status' => true, $this->ci->auth->getUserCredential()] : ['status' => false, $this->ci->auth->getLoginMessage()];
    }
}